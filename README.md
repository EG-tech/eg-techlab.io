Back-end stuff for my personal website/CV. Don't look here. Go directly to (https://ethan-gates.com), do not pass go, do not collect $200.

Built using:
- [Hugo](https://gohugo.io/)
- [Anatole theme](https://github.com/lxndrblz/anatole/)
- social media icons from [Font Awesome](https://fontawesome.com/), [Fork Awesome](https://forkaweso.me/Fork-Awesome/) and [Academicons](https://jpswalsh.github.io/academicons/)
