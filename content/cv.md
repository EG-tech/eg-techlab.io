---
aliases:
  - resume
author: Ethan
---

# Education

* B.A. in English and Russian, concentration in Film Studies, *Amherst College*, 2012
* M.A. in Moving Image Archiving and Preservation, *New York University*, 2015

<br>

# Work experience

* July 2024 - present: **Senior Software Preservation and Emulation Technologist**, *Yale University Library*
  * Duties include:
    * Creating user documentation that supports the sustainability and adoption of Emulation-as-a-Service Infrastructure (EaaSI)
    * Leading training initiatives to increase the capacity of cultural heritage and research practitioners to leverage software preservation and emulation infrastructure
    * Leading testing and troubleshooting of new EaaSI platform releases and translating feedback into user stories that guide the development roadmap and maintenance
    * Conducting research and leading initiatives to assess the usability of software preservation and emulation infrastructure
    * Working with the Software Preservation and Emulation unit, building replicable software preservation policies and workflows at Yale
    * Working with EaaSI's organizational partners, facilitating communication and learning among global EaaSI users
  * Supervisor: Wendy Hagenmaier (July 2024 - present)
* October 2018 - June 2024: **Software Preservation Analyst**, *Yale University Library*
  * Duties included:
    * As User Support Lead for the grant-funded EaaSI (Emulation-as-a-Service Infrastructure) program of work:
      * Coordinating prompt response to issues and bugs reported by EaaSI Network members/users
      * Developing training materials and resources for using EaaSI and emulation for software preservation
      * Guiding new and existing EaaSI Network members on available solutions for their software and digital preservation use cases
      * Providing feedback and synthesis of community response to aid in feature development, design, and prioritization for the EaaSI service
    * Configuring and troubleshooting legacy software within Yale and the EaaSI network's instances of Emulation as a Service
    * Supervised Yale student team responsible for software configuration, description, and QC
  * Supervisor: Seth Anderson (October 2018 - November 2022), Euan Cochrane (November 2022 - August 2023), Wendy Hagenmaier (August 2023 - June 2024)
* August 2017 - present: **Freelance Workshop Leader**
  * Duties include:
    * Designing and leading occasional workshops for library, information and other cultural heritage professionals on topics of digital literacy and audiovisual archiving.
    * Topics have included: using GitHub, using emulators, troubleshooting analog A/V equipment
    * Organizations worked with include: METRO (Metropolitan New York Library Council), the NYU Institute
      of Fine Arts, Memory Lab Network
* November 2015 - June 2018: **Moving Image Archiving and Preservation Technician**, *New York University*
  * Duties included:
    * Coordinated use of department's archival labs and equipment
    * Troubleshot archival equipment and workflows, coordinated repairs and purchasing as needed
    * Trained students in proper use and handling of film scanning, audiovisual digitization, and digital forensics equipment
    * Consulted with faculty on utilization of equipment and software in department coursework, as well as implementation of technological developments in professional field
    * Designed and taught a series of technical workshops to supplement program curriculum
  * Supervisors: Ann Harris, Scott Statland
* Summer 2016: **Preservation Consultant**, *The Jewish Museum*
  * Duties included:
    * Surveyed inventories, condition assessments and current catalog system for analog audiovisual holdings
    * Summarized current professional best practices for digitization and digital preservation
    * Presented short- and long-term recommendations for mass digitization and digital repository management
  * Supervisors: Aviva Weintraub, Jaron Gandelman
* Fall 2015: **Preservation Consultant**, *BB Optics*
  * Duties included:
    * Conducted physical assessment of a filmmaker’s Super 8 film collection.
    * Provided the filmmaker with a report on short- and long-term measures for digitization and both physical and digital preservation of the collection’s assets.
  * Supervisors: Bill Brand
* Fall 2013 - Fall 2015: **Audiovisual Assistant**, *New York University Department of Cinema Studies*
  * Duties included:
    * Carried out inspection, repair and digitization of 16mm film and audio cassette collections in the department's Film Study Center archive.
    * Cataloged new acquisitions, derivative files and preservation actions in FileMaker Pro database.
    * Assisted professors and students with classroom A/V setup and projection (digital and 16mm).
  * Supervisor: Ann Harris
* Fall 2014: **Special Collections Intern**, *City College of New York Libraries*
  * Duties included:
    * Cataloged a collection of over 800 videotapes in Archivist's Toolkit
    * Submitted proposal for full digitization of the collection, including vendor recommendations and costs
  * Supervisor: Prof. Sydney Van Nort
* Summer 2014: **Archives and Collection Management Intern**, *Northeast Historic Film*
  * Duties included:
    * Vault management
    * Cataloged new acquisitions and access copies in Collective Access database
    * 16mm film inspection and repair
  * Supervisor: David Weiss
* Spring 2014: **Film & Video Archive Intern**, *Major League Baseball Productions*
  * Duties included:
    * Inspected a 16mm film collection for condition and descriptive content
    * Performed audio cassette digitization
  * Supervisor: Nick Trotta
* Spring 2013: **Research Assistant**, *Department of Political Science, Amherst College*
  * Duties included:
    * Compiled bibliography/citations in Zotero
  * Supervisor: Prof. William Taubman
* Fall 2012 - Spring 2013: **Center for Russian Culture Assistant**, *Department of Russian, Amherst College*
  * Duties included:
    * Created finding aids for over a dozen uncataloged paper collections
    * Digitally photographed aging/delicate print materials
    * Coordinated and provided reference assistance for visiting scholars
    * Curated rotating public display of collection holdings
  * Supervisor: Prof. Stanley Rabinowitz

# Volunteering

**Active**

* *[Open Source Committee, Association of Moving Image Archivists](https://github.com/amiaopensource/)*
  * Creator and maintainer of [The Cable Bible](https://amiaopensource.github.io/cable-bible/), 2016 - present
* *[Born Digital Archives Advisory Group, Yale University Library](https://web.library.yale.edu/committees/born-digital-archives-advisory-group)*
  * Member, July 2024 - present

**Past**

* *[Membership Committee, BitCurator Consortium](https://bitcuratorconsortium.org/about/membership-committee/)*
  * Member, July 2022 - May 2023
* *[Community Engagement Collaborative, Software Preservation Network](https://www.softwarepreservationnetwork.org/core-activities/community-engagement/)*
  * Co-coordinator, March 2021 - December 2021
  * Member, January 2021 - May 2023
* *[Educators Membership Working Group, BitCurator Consortium](https://bitcuratorconsortium.org/call-for-volunteers-bcc-educators-membership-working-group/)*
  * Member, January 2022 - June 2022
* *[Digital Formats Subcommittee, Continuing Education Advocacy Task Force, Association of Moving Image Archivists](https://amianet.org/committees/task-forces-and-working-groups/continuing-education-advisory-task-force/)*
  * Member, March 2021 - March 2022
* *[Continuing Education Advocacy Task Force, Association of Moving Image Archivists](https://amianet.org/committees/task-forces-and-working-groups/continuing-education-advisory-task-force/)*
  * Member, April 2020 - August 2020
* *[Policy & Procedure Committee, Software Preservation Network](https://www.softwarepreservationnetwork.org/governance/)*
  * Member, April 2019 - March 2020
* *[XFR Collective](https://xfrcollective.wordpress.com)*
  * Core member, Summer 2016 - May 2018

# Projects

* [The Cable Bible](https://amiaopensource.github.io/cable-bible/)
  * A visual guide to cables and connectors used for audiovisual preservation
* [Emulation Bibliography](https://www.zotero.org/ethan-gates/collections/ERZIYJ3T)
  * A public Zotero collection of academic, professional, and narrative works on the topic of emulation and its utility in digital preservation
* [emulation-resources](https://gitlab.com/EG-tech/emulation-resources)
  * A public portal of links useful for emulating for software preservation and access (including emulator software, retrocomputing forums and download sites, tutorials, and more)
* [disk-imaging-resources](https://gitlab.com/EG-tech/disk-imaging-resources)
  * A companion portal to emulation-resources, but for resources and projects related to creating and manipulating disk images
* [The Patch Bay](https://patchbay.tech/)
  * Narrative blog connecting topics of preservation and technology
* [QEMU QED](https://eaasi.gitlab.io/program_docs/qemu-qed/)
  * The missing QEMU manual for digital preservation
* [Qiwi](https://gitlab.com/EG-tech/qiwi)
  * an open-source graphical launcher for [QEMU](), written in Python

# Skills

* Troubleshooting
  * Analog film equipment
  * Analog video equipment
  * Digital hardware/software
* Writing
  * Technical documentation
  * Academic research
  * Blogging/narrative
* Communication
  * Written
  * Public speaking and presentations
  * Internal/team collaboration
* Instruction
  * Workshop and lab design
  * Classroom demonstration and lectures
  * Small group/one-on-one tutoring
* Community facilitation
  * Group discussion and consensus techniques (e.g. progressive stack)
  * Drafting and enforcing Codes of Conduct
  * Panel organization and moderation
* Languages
  * English (fluent)
  * Russian (proficient)
* Programming and markup languages
  * Bash
  * Python
  * HTML/CSS
  * Markdown, reStructured Text
  * SQL
* Git hosting platforms
  * GitHub, GitLab
* Static site generators
  * Jekyll
  * Hugo
  * Sphinx
* Media post-production
  * Adobe Creative Suite
  * Audacity
  * Final Cut Studio
  * Kdenlive
  * ffmpeg
  * DIAMANT Dustbuster+
* Office productivity suites
  * Microsoft Office suite
  * LibreOffice
  * G Suite (Google)
* Film handling and repair
* Audiovisual collection assessment
