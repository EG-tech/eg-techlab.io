---
title: "Using Emulators to Resurrect Recovered Data"
date: 2020-01-27
---

Co-facilitated a three-hour workshop on using emulator applications to install legacy operating systems and recover
data or run applications contained on disk images.

This workshop was co-designed and led by Claire Fox, NYU-MIAP graduate student (class of '20) and former
Yale/EaaSI intern. The target audience for this workshop was METRO's core community of archivists, librarians,
and creators, and aimed to impress both basic computing concepts essential to emulation as well as impart
larger concerns and efforts to address software preservation.

All associated workshop materials are available in a shared Google Drive folder at <https://bit.ly/metroemulationworkshop>. Slides
provided below.

[![](/images/Using_Emulators_to_Resurrect_Data.png)](/files/Using_Emulators_to_Resurrect_Data.pdf)
