---
title: "Under the Hood: Troubleshooting and Diagnosing Equipment Failure"
date: 2017-02-22
---

Part of self-designed Talking Tech workshop series for Moving Image Archiving and Preservation students.

What’s that sound? If you’re asking that question in the archive, the answer is probably: sadness. But self-repairing your equipment can save you money, save the planet, and at the very least, teach you something you didn’t know yesterday! Let’s look at some broken decks!

A hands-on session based on diagnosing actual broken analog video equipment. No slides or recording available.
