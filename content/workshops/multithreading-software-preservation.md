---
title: "Multithreading Software Preservation"
date: 2019-08-02
---

A full-day software preservation and emulation workshop.

Taught in conjunction with the Society of American Archivists Annual Meeting, "Multithreading Software Preservation: A Software Preservation and Emulation Workshop" was a collaboration between the EaaSI and Fostering a Community of Practice projects, both affiliates of the Software Preservation Network.

Co-taught and facilitated with Jessica Meyerson, Seth Anderson, Cynde Moya, Tracy Popp, Michael Olson and Lauren Work. Full event details contained on the [SPN web site](https://www.softwarepreservationnetwork.org/blog/aug-2019-software-preservation-emulation-workshop/), and collaborative notes from the day are [available](https://bit.ly/swpres-emulation).
