---
title: "Archiving and Preserving for Indie Filmmakers: DIY Tips from the XFR Collective"
date: 2016-11-16
---

Workshop co-organized by Third World Newsreel, the Documentary Forum at City College of New York, and XFR Collective. Co-led with members of XFR Collective.

Tips and strategies for independent filmmakers and students for preserving their works. Topics will include: best practices, physical media management/care, prioritization and planning, and digitization/capture strategies.
