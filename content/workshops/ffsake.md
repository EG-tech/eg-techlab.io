---
title: "ffsake: further fluff with ffmpeg"
date: 2018-02-05
---

Part of self-designed Talking Tech workshop series for Moving Image Archiving and Preservation students.

Possibly the most powerful software available for manipulating, editing and transcoding audiovisual data also just happens to be free and open source. That’s great for access, and great for preservation - but doesn’t make using it any easier! Come play in the command line and find out what ffmpeg can do for you.

[![](/images/ffsake_workshop_2018.png)](/files/ffmpeg_workshop_2018.pdf)
