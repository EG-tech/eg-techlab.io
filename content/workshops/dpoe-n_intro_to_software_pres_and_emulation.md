---
title: "An Introduction to Software Preservation and Emulation"
date: 2024-05-07
---

A two-session, four-hour virtual workshop, co-designed and taught with Claire Fox for the Digital Preservation Outreach and Education Network (DPOE-N). Originally held on May 7 and May 9, 2024.

What do you do if you get data off old floppies, CD-ROMs, or hard drives, but still can’t access the files because they depend on decades-old software? What if the files lose data or don’t look right when converted or opened in a modern format? Emulation can help by recreating the whole computing environment your data was originally intended for. But it also requires a familiarity with software and computing systems that takes significant time and is often outside the bounds of traditional archiving and cultural heritage work.

In this two-day workshop, participants and instructors will take a step back from our day-to-day work and consider the question: What is a computer? Using this question as an anchor, participants will gain conceptual and practical experience with the concerns of incorporating software preservation and emulation into digital preservation workflows.

On day one, participants will gain perspective on the landscape of software and software-dependent collections and build basic familiarity with components of computers and virtual machines. On day two, we’ll work directly with emulation and digital preservation tools, and take time at the end of the day to reflect on the role of emulation as a preservation and access strategy within a cultural heritage context.

- [Workshop Materials](https://drive.google.com/drive/folders/1iAD-rnveug0wOmp2YF8M0uO082bzCpT5) (including slide deck and activity instructions)
- [Day 1 Recording](https://talks.pratt.edu/media/t/1_ui88wgfs)
- [Day 2 Recording](https://talks.pratt.edu/media/t/1_j8gqzi2y) [Audio en español Dia 2](https://talks.pratt.edu/media/t/1_sveg8eig)