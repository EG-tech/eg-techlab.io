---
title: "Preparation and Process: Software Preservation and Emulation for Research Data"
collection: teaching
type: "Workshop"
permalink: /workshops/2020-spring-workshops-2
venue: "IDCC"
date: 2020-02-17
location: "Croke Park, Dublin, Ireland"
---

Co-facilitated a full day workshop in conjunction with the 15th International Digital Curation Conference.

During this workshop, leaders and participants from the EaaSI and FCoP SPN-affiliate projects presented 
use case examples and hands-on exercises to convey technical knowledge about emulation services and 
encourage organizational reflection, policy development, and capacity-building. Facilitators focused 
on real-world examples of emulation at their organizations, with a particular interest in Research Data
Management, data archives, and the needs of digital humanities and scientific community patrons.

Detailed workshop description and overview available at <http://www.dcc.ac.uk/events/idcc20/workshop1>
