---
title: "Digital Preservation: Caring for Digital Art Objects"
date: 2019-10-11
---

Co-facilitated two days of a three-day workshop for students and professional conservators working with digitized, born-digital, audiovisual or computer-based artworks.

The workshop was led by Nicole Martin (Human Rights Watch, NYU), and co-taught with Amy Brost (MoMA) and Jonathan Farbowitz (Guggenheim). In addition to offering general lab assistance and advice, I led a session and lab activity introducing participants to key technical and strategic concepts of emulation, virtualization, and software preservation for maintaining the context of digital objects.

Workshop description and details available on the [IFA's site](https://ifa.nyu.edu/conservation/tbm-wkshp-oct19.htm).
