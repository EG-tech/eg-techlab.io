---
title: "Hack the Docs: Technical Documentation and Communicating with IT"
date: 2017-04-12
---

Part of self-designed Talking Tech workshop series for Moving Image Archiving and Preservation students.

Being able to talk about tech can be just as important as being able to use it. We’ll discuss how to design and document clear workflows that others can replicate, and how to get quick solutions from sysadmins, engineers, or tech support for those times you get in over your head!

[![](/images/hackthedocs.png)](https://www.nyu.edu/tisch/preservation/program/modules/talkingtech_workshops/talkingtech3_2017spring_technical_docs.pdf)
