---
title: "Why Are All of Hollywood's Leading Men So Old?"
date: 2015-02-17
---
Published in *The New Republic* online.

A trio of films positioning Jack O'Connell as a new leading man lays bare an interesting trend: the industry's male stars are getting older, with no obvious generation up-and-coming to replace them.

[Read here](https://web.archive.org/web/20150219010214/https://newrepublic.com/article/121004/jack-oconnell-and-hollywoods-arrested-development)

![](/images/jack-oconnell.png)
