---
title: "Son of Saul: Not Just Another Holocaust Movie"
date: 2016-02-17
---
Published in *The New Republic* online.

Laslzo Nemes' film "Son of Saul" is a self-reflective rumination on cultural obsession with the Final Solution

[Read here](https://newrepublic.com/article/130019/son-saul-not-just-another-holocaust-movie)

![](/images/son-of-saul.png)
