---
title: "Why Hollywood Keeps Awarding Movies About Movies"
date: 2015-02-23
---
Published in *The New Republic* online.

The Best Picture win for "Birdman" reveals the film industry's insecurities in the modern landscape of entertainment.

[Read here](https://web.archive.org/web/20150224090816/https://newrepublic.com/article/121127/oscars-new-love-movies-about-movies-shows-hollywoods-anxiety)

![](/images/birdman.png)
