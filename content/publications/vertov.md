---
title: "Dziga Vertov: The Man with the Movie Camera and Other Newly-Restored Works"
date: 2015-06-25
---
Published in *Diabolique Magazine* online.

The release of Flicker Alley's definitive Blu-Ray box set prompts some thoughts on Vertov's much-discussed classic.

[Read here](https://web.archive.org/web/20151231083934/http://diaboliquemagazine.com/dziga-vertov-the-man-with-the-movie-camera-and-other-newly-restored-works-u-s-blu-ray-review/)

![](/images/vertov.png)
