---
title: "Towards a Universal Virtual Interactor (UVI) for Digital Objects"
date: 2019-09-17
---
Co-author on a paper describing history, need, and requirements for the "Universal Virtual Interactor": an EaaSI-sponsored
API allowing for automated interaction with digital objects in their original software.

Presented by Euan Cochrane and Klaus Rechert to the 16th International Conference on Digital Preservation (iPRES 2019)
at the EYE Film Museum in Amsterdam, The Netherlands, as part of the "Cutting Edge // Emulation" stream.

[Read here](https://ipres2019.org/static/pdf/iPres2019_paper_128.pdf)
