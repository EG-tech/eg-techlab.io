---
title: "The Handbook of Archival Practice"
date: 2021-09-01
publishdate: 2021-08-10
---
Contributed the entry for "Emulation" to [The Handbook of Archival Practice](https://rowman.com/ISBN/9781538137345/The-Handbook-of-Archival-Practice) (forthcoming September 2021).

Gates, Ethan. "6.11 Emulation". Patricia C. Franks, ed. *The Handbook of Archival Practice*. Rowman & Littlefield: 2021.
