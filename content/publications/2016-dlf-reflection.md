---
title: "AMIA/DLF Hack Day Fellow Reflection"
date: 2016-12-01
---
Written for the Digital Library Federation blog.

Thoughts from the 2016 AMIA/DLF Hack Day event, which I attended as a 'virtual cross-pollinator'.

[Read here](https://web.archive.org/web/20200707140028/https://www.diglib.org/fellow-reflection-ethan-gates/)
