---
title: "Oscar-Nominated 'Timbuktu' Shows the Terrors of Life Under Islamist Extremism"
date: 2015-02-09
---
Published in *The New Republic* online.

A review of Abderrahmane Sissako's "Timbuktu", the first feature nominated for Best Foreign Language Film at the Oscars to be directed by a black, African-born director. The perils of jihadist rule in an African village treated with unequal humanity and a terrible beauty.

[Read here](https://web.archive.org/web/20150502040359/http://www.newrepublic.com/article/121015/timbuktu-mauritanias-oscar-nominee-shows-life-under-terrorism)

![](/images/timbuktu.png)
