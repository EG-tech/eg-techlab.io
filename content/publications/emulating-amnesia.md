---
title: "Emulating Amnesia"
date: 2020-01-06
---
A post for "Saving Digital Stuff", a blog about born-digital archival collections at Yale
University. Co-written with Alice Prael, Digital Archivist for Yale Special Collections, Beinecke
Rare Book & Manuscript Library.

In this post, we discuss the disk imaging and evaluation process (via emulation) for offering access
to *Amnesia*, a text-adventure game written by author Thomas M. Disch in the 1980s and distributed for
Commodore, IBM PC and Apple II. The game was included with Disch's papers, acquired by the Beinecke,
and offers some unique challenges for access and context alongside the Disch collection.

[Read here](https://web.archive.org/web/20200123044043/http://campuspress.yale.edu/borndigital/2020/01/06/emulating-amnesia/)
