---
title: "Preserving and Integrating Community Knowledge of Computing Systems"
date: 2019-11-07
---
A post for the Digital Preservation Coalition blog as part of the World Digital Preservation Day
2019 celebration.

A call to action on preserving and integrating a broader swath of resources related to 
using legacy software and computers into digital access systems, the better to capture communal, ephemeral
and experiential knowledge of systems rather than just technical documentation.

[Read here](https://web.archive.org/web/20200409134357/https://www.dpconline.org/blog/idpd/preserving-and-integrating-community-knowledge)
