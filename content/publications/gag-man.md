---
title: "Once a Joke Goes Viral, Who Cares Where It Came From?"
date: 2015-09-09
---
Published in *The New Republic* online.

A review of "The Gag Man", Matthew Dessem's biography of Clyde Bruckman, inspires second thoughts on virality and attribution in modern comedy.

[Read here](https://web.archive.org/web/20150915060924/https://newrepublic.com/article/122765/once-joke-goes-viral-who-cares-where-it-came)

![](/images/gag-man.png)
