---
title: "In Iran, Turning on a Camera Is An Act of Subversion"
date: 2015-10-01
---
Published in *The New Republic* online.

A review of "Taxi", the latest work of cinematic resistance by Iranian filmmaker Jafar Panahi.

[Read here](https://web.archive.org/web/20151002151857/https://newrepublic.com/article/122980/iran-turning-camera-act-subversion)

![](/images/taxi.png)
