---
title: "Usable Software Forever: The Emulation as a Service Infrastructure (EaaSI) Program of Work"
date: 2022-10-19
---
Co-author on a paper describing the background, goals, and accomplishments to date of the grant-funded EaaSI program of work. 

Presented by Euan Cochrane to the 18th International Conference on Digital Preservation (iPRES 2022)
in Glasgow, Scotland, Oct. 19, 2022.

[Read here](https://osf.io/ay48c/)
