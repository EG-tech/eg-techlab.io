---
title: "The Oscar-Nominated Film Hollywood Loves, But the Russian Government Hates"
date: 2015-02-08
---
Published in *The New Republic* online.

A review of Andrei Zvyagintsev's film "Leviathan", contextualized within Minister of Culture Vladimir Medinsky's aborted attempts at international cultural outreach.

[Read here](https://web.archive.org/web/20150209043528/https://newrepublic.com/article/120804/leviathan-oscar-nominee-foreign-film-exposes-russian-corruption)

![](/images/leviathan.png)
