+++
title = "Welcome!"
date = "2024-07-10"
author = "Ethan"
+++

My name is Ethan Gates, and I currently live (and work from home) in Central Massachusetts. 
 
Broadly speaking, I am passionate about professional and community education around audiovisual and digital technology. I like to learn _how_ stuff works - whether that means taking apart a Betacam deck or tinkering in a computer's command line interface - and share what I've found out with others. That's come to mean leading workshops, writing blog posts, volunteering with community archiving efforts like [XFR Collective](https://xfrcollective.wordpress.com), and connecting with many other amazing and thoughtful archivists, information professionals, maintainers and technologists.
 
I currently work in the Software Preservation and Emulation unit at Yale University Library. My primary focus is on user documentation, training, testing, and developing requirements for the EaaSI (Emulation-as-a-Service Infrastructure) remote emulation platform, building off about six years of effort during EaaSI's grant-funded seed period. But in general as a "technologist" I do what I can to make emulation and software preservation more accessible practices for heritage organizations.

Before my time at Yale I worked as the Moving Image Archiving and Preservation Technician for the Department of Cinema Studies at New York University. I was responsible for maintenance and use of the lab spaces and equipment employed by the department's two-year Master's degree program in Moving Image Archiving and Preservation (of which I am also a graduate, class of 2015).

My undergraduate background at Amherst College ('12) was in film studies and Russian language/literature, which will hopefully explain the errant Soviet cinema references you may encounter. I've occasionally published film reviews and still like to indulge my scholarly bent - apologies in advance if I ever over-enthusiastically yell at you about Boris Shumyatsky and/or the history of film preservation in the Soviet Union.

Please feel free to peruse my [full CV](/cv/), look over some of my [workshop materials](/workshops/), [talks](/talks/), and [published articles](/publications/), or check out my professional blog, [The Patch Bay](https://patchbay.tech). Thanks for visiting, and get in touch any time!
