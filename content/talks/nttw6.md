---
title: "Challenges Building an Emulation Service for Born-Digital Collections"
date: 2022-10-28
---

Remote presentation to the ["No Time to Wait 6"](https://mediaarea.net/NoTimeToWait6) conference hosted by Netherlands Institute for Sound & Vision at The Hague (Oct. 26-28, 2022)

Inspired by this year’s theme of Transparency and Trust: rather than talk as much about the EaaSI platform itself, in this lightning talk I address some of our biggest challenges in the EaaSI program. The truth is that we will not get closer to the program's vision of seamlessly incorporating emulation into access workflows across the field without serious, coordinated effort and advocacy in a few key areas.


[![](/images/NTTW6.png)](/files/NTTW6.pdf)
