---
title: "Do You Have a DSS?: Digital Storage Strategy, An Introduction"
date: 2017-03-21
---

Lecture presented as part of MIAP's Collection Management course.

An overview of digital storage strategy for media preservation, including risk factors and causes of digital loss, storage media and characteristics, physical interfaces and data transfer protocols, types of external storage, administrative strategy, and failure and data recovery.

[![](/images/digitalstoragestrategy.png)](/files/Digital_Storage_Strategy_lecture.pdf)
