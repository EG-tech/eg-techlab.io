---
title: "Pay It Backward: Challenges and Opportunities for Implementing Open-Source Emulators in Institutional Workflows"
date: 2024-08-22
---

Since 2018, the Mellon- and Sloan-funded EaaSI program of work has focused on scaling use of the Emulation-as-a-Service framework (an open, web-based platform for managing and accessing remote emulators) for digital preservation in cultural heritage and memory institutions. In doing so, the program has built and relied on decades of effort by open-source emulator developers in the video game and retro-computing communities.

This presentation will address challenges encountered and lessons learned from the EaaSI team regarding this to-date largely one-way relationship and dependence of generalized, institutional software preservation workflows on gaming archivists and hobbyists. This will touch on technical limitations – the difficulties of centralizing wildly disparate systems and codebases; features of gaming-focused emulators that prove problematic for preserving general-purpose computing environments and vice versa – but perhaps more importantly, administrative difficulties involved in long-term support, funding, and maintenance for open-source emulation in the current landscape of academic and government archives in the U.S.

[![](/images/Pay_It_Backward_Strong_symposium_08-22-2024.png)](/files/Pay_It_Backward_Strong_symposium_08-22-2024.pdf)
