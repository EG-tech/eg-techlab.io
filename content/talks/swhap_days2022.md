---
title: "EaaSI Access to Legacy Software: Designing Emulation Services for the Future"
date: 2022-10-20
---

Remote presentation to the ["SWHAP Days"](https://www.softwareheritage.org/news/events/swhap_days_2022/) workshop (hosted by Software Heritage in Paris, Oct. 19-20, 2022)

Since 2018, the grant-funded EaaSI program of work has been building off of the bwFLA project’s Emulation-as-a-Service framework to investigate and encourage emulation as a critical missing link in software and digital preservation workflows. This presentation summarized the program’s goals, the current state of EaaSI platform and service design, and the challenges that remain for making EaaSI and emulation a seamless piece of collections access.

[![](/images/SWHAP_Days2022.png)](/files/SWHAP_Days2022.pdf)
