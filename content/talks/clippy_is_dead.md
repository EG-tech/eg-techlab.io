---
title: "Clippy Is Dead, Long Live Clippy: Emulation as an Access Strategy"
date: 2021-05-25
---

Remote lecture presented to the METRO community in a series of webinars on the topic of digital preservation.

This talk is essentially an updated version of ["Emulation 101"](https://ethan-gates.com/2020/01/emulation-101_emulation-as-strategy/), with edits to account for advances in the EaaSI program of work, new demos of emulation and the EaaSI platform, and expanded discussion of rendering. 

[![](/images/Clippy_is_Dead.png)](/files/Clippy_is_Dead.pdf)
