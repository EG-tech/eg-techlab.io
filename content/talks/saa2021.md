---
title: "Poster Session: Initial Findings from the 2021 SPN Hosted Emulation Services Pilot"
date: 2021-08-02
---

Poster presented virtually to the Society of American Archivsts' 2021 ARCHIVES*RECORDS annual meeting.

The poster provides an overview and some intial conclusions from the joint hosted emulation services pilot provided by the Software Preservation Network and the EaaSI program of work. 14 participant organizational members of SPN have been provided with a cloud-hosted version of the EaaSI platform for designing use cases, engaging local communities, and brainstorming further efforts needed to bolster the role of emulation in preservation workflows.

[![](/images/saa2021.png)](/files/SAA_2021_initial_findings_SPN_pilot_EG.pdf)
