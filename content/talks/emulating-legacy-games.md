---
title: "Emulating Legacy Games"
date: 2018-02-20
---

Class lecture and accompanying lab exercise presented to the Handling Complex Media course for the NYU-MIAP program.

A practical introduction to setting up and troubleshooting emulation/virtualization on modern hardware for legacy computing systems. Classic video games used as examples. Demonstrated installation and setup of DOSBOX (MS-DOS), VirtualBox (Windows 95) and SheepShaver (Mac OS 9).

[![](/images/emulating_legacy_games.png)](/files/Emulating_Legacy_Games.pdf)
