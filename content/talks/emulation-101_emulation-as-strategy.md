---
title: "Emulation 101 - Emulation as a Strategy"
date: 2020-01-22
---

Remote lecture presented to the METRO DigiPres Meetup as part of a two-hour "Disk Imaging and Emulation 101" event.

My talk was a portion of an event on disk imaging and emulation for libraries and archives, which also
included contributions from Erin Barsan and Rachel Egan of Small Data Industries, as well as Claire Fox
(NYU-MIAP student and former EaaSI intern). "Emulation 101 - Emulation as a Strategy" introduced key
technical concepts and history of emulation, as well as a brief survey of using emulation for access services
in digital archives, and finally summarized the work of EaaSI and the EaaS platform to make emulation
more accessible for METRO's community of GLAM professionals.

[![](/images/Emulation_101_Emulation_as_Strategy.png)](/files/Emulation_101_Emulation_as_Strategy.pdf)
