---
title: "Poster Session: The Cable Bible - A Guide to Connecting Audiovisual Equipment"
date: 2016-11-11
---

Poster presented at the annual conference of the Association of Moving Image Archivists, in support of [The Cable Bible](https://amiaopensource.github.io/cable-bible).

The Cable Bible is a comprehensive online guide to identifying cables and connectors used for audiovisual tech and media preservation that I began as a resource to the professional community. During the poster session, I outlined the project as it stood thus far and laid out future goals and milestones to AMIA conference attendees.

![](/images/Cable_Bible_poster.jpg)
