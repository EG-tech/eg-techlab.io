---
title: "UniCLI Powerful: An Introduction to the Command Line"
date: 2017-12-11
---

Class lecture and accompanying exercise presented to the Digital Literacy course for the NYU-MIAP program.

A first-timer's introduction to understanding and using command line interfaces and the Bash shell.

[![](/images/CLI_intro.png)](/files/CLI_intro.pdf)
