---
title: "EaaSI Roundtable: What We Talk About When We Talk About Emulation"
date: 2021-02-09
---

A roundtable discussion featuring guest speakers from around the EaaSI Network presenting their emulation use cases and discussing moments of interaction between researchers, practitioners, and emulation technology in digital archives.

This panel was sponsored and recorded by the EaaSI program and featured Eric Kaltman (CSU Channel Islands), Tracy Popp (University of Illinois) and Fernando Rios (University of Arizona) as guest speakers. My role in the roundtable was moderator and facilitator, prompting our speakers with questions to elaborate on their use cases and respond to each other.

The roundtable recording and supplementary materials are available at:
https://www.softwarepreservationnetwork.org/eaasi-roundtable-1/
