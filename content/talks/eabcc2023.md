---
title: "Preserving Access to Email Archives via Remote Emulation"
date: 2023-06-14 
---

Lightning talk presented at the Email Archiving Symposium (June 12-14, 2023) sponsored by the Email Archives: Building Capacity and Community grant program.

The grant-funded EaaSI (Emulation-as-a-Service Infrastructure) program of work, hosted by Yale University Library, has been working for the past 5 years to build an open-source access platform for remote access to legacy software and data in emulation. Since the underlying technology of the platform can be used to authentically render and interact with a huge variety of domain-specific applications and collections, the team frequently uses case studies to gather feedback and drive development, making sure feature and workflow design meets the real-life needs of archivists, curators, and researchers.

This lightning talk will focus specifically on EaaSI’s investigations into using emulation for access to email archives contained within archival and special collections. A recent Yale patron request that included access to a prominent legal scholar’s mid-2000s Outlook data, sparked discussion between special collections staff and the EaaSI team regarding both the benefits and limitations of using the EaaSI platform as a type of “virtual reading room”. We will cover the current state of the platform and the degree to which it met both staff and patron’s needs in this use case, as well as planned development goals the EaaSI team has drafted to better address working with email archives in the future.

I hope this presentation will simultaneously inspire attendees to consider/evaluate how emulation could become a practical part of their own email assessment and access workflows, and for the community around this symposium to dream big together: what would the ideal access platforms of the future look like?

[![](/images/06-14-23_Email_Archiving_Symposium.png)](/files/06-14-23_Email_Archiving_Symposium.pdf)
