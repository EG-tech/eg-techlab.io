---
title: "Investigating Emulation as a Service for Reproducible Research at Yale"
date: 2020-01-28
---

Co-presented with Limor Peer at the virtual "ReproLibs" event, on the topic of EaaS and its various
approaches/possibilities for reproducing studies from the Yale ISPS Data Archive.

Limor presented the problem of computational reproducibility and access facing data sets and studies
deposited in the Yale Institute for Social and Policy Studies' Data Archive. I presented three possible
technical solutions for reproducibility using the EaaS platform and advances from the EaaSI program of work,
including manual recreation of suitable emulations, automated matching with the UVI, and the possibility
of direct container import for already-packaged studies.

[Slides and recording available on OSF.](https://osf.io/tejfw/)
