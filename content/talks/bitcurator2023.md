---
title: "Qiwi: Building a New Open Source App for Archivists"
date: 2023-03-29
---

At BUF 2021, I presented a lightning talk on potential archival and curation uses for QEMU, an open source emulator. As a follow up, I will present Qiwi (https://gitlab.com/eg-tech/qiwi), a graphical Python application I am developing that builds on top of QEMU, with the explicit goal of easing the use of QEMU in archival appraisal.

While I will briefly demo the current state of Qiwi’s functionality, I will also use this talk to consider concerns of labor and sustainability in open source development from my new (and inexperienced) perspective of solo developer/maintainer on an application. Can Qiwi avoid becoming Yet Another DigiPres Tool reliant on a single enthusiastic but overwhelmed individual? How far can (or should) I take an attempt at professional and personal growth that straddles the line of my day-to-day duties?

[![](/images/BF2023_Qiwi.png)](/files/BF2023_Qiwi.pdf)
