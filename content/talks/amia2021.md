---
title: "Intro to Digital Preservation & Formats"
date: 2021-01-27
---

Collaborated and presented as part of a team for a webinar, sponsored by the Association of Moving Image Archivists, on the topic of "digital preservation formats." Co-organizers included Nicole Martin (Human Rights Watch), Annie Schweikert (Stanford Libraries), and Jackie Jay (Farallon Archival Consulting LLC).

In addition to general feedback and review of the overall structure and sequencing of the two-hour session, I provided a brief lecture on storage media encountered for and in digital preservation, titled "Digital is Analog, Too: A Brief Guide to Digital Storage Formats." This covered some of the history and pros/cons of various digital storage technologies like spinning disk, floppy disk, optical media and magnetic tape. Slides for this portion of the webinar are located below; the recording of the full session can also be rented on-demand from AMIA: https://vimeo.com/ondemand/digiformats. 

[![](/images/Storage_Media_Formats_EG.png)](/files/Storage_Media_Formats_EG.pdf)
