---
title: "Best Things of 2020"
date: 2020-12-28
---

My adolescent enthusiasm for awards shows and year-end movie lists has devolved, inexorably, to whatever 2020 is/was. Some things brought me joy - many just passed the time.

Rather than a detailed breakdown, here are some recommendations, the best things I watched/read/listened to in this awful year. Note that, other than movies (where the spirit and bureaucracy of the [EMOs](https://bestfilmsofourlives.blog/2018/01/07/the-11th-annual-emos/) still lives on inside me, even if I could barely muster the energy to fire up the new Claire Denis or Kelly Reichardt flick *before* a pandemic), these are all pieces of culture that I consumed in 2020, not necessarily released this year. I still have a stack of new-in-2020 movies that I want to see and promise I'll definitely get to sometime after we re-watch all the way through Peep Show for the fourth time.

Fiction
---------
- [The City We Became](https://bookshop.org/books/the-city-we-became/9780316509848), N.K. Jemisin
- [Girl, Woman, Other](https://bookshop.org/books/girl-woman-other-a-novel-booker-prize-winner/9780802156983), Bernardine Evaristo
- [How Much of These Hills Is Gold](https://bookshop.org/books/how-much-of-these-hills-is-gold/9780525537205), C Pam Zhang
- [Kindred](https://bookshop.org/books/kindred-6268c8de-13c6-4936-b889-3432f8682d05/9780807083697), Octavia Butler
- [The Lathe of Heaven](https://bookshop.org/books/the-lathe-of-heaven/9781416556961), Ursula K. Le Guin
- [The Likeness](https://bookshop.org/books/the-likeness-9789573325932/9780143115625), Tana French
- [The Orphan Master's Son](https://bookshop.org/books/the-orphan-master-s-son/9780812982626), Adam Johnson
- [Pachinko](https://bookshop.org/books/pachinko-national-book-award-finalist/9781455563920), Min Jin Lee


Non-Fiction
------------
- [Black Software: The Internet & Racial Justice, from the AfroNet to Black Lives Matter](https://bookshop.org/books/black-software-the-internet-racial-justice-from-the-afronet-to-black-lives-matter/9780190863845), Charlton D. McIlwain
- [Glitch Feminism: A Manifesto](https://bookshop.org/books/glitch-feminism-a-manifesto/9781786632661), Legacy Russell
- [How to Do Nothing: Resisting the Attention Economy](https://bookshop.org/books/how-to-do-nothing-resisting-the-attention-economy/9781612197494), Jenny Odell
- [The Innovation Delusion: How Our Obsession with the New Has Disrupted the Work That Matters Most](https://bookshop.org/books/the-innovation-delusion-how-our-obsession-with-the-new-has-disrupted-the-work-that-matters-most/9780525575689), Lee Vinsel and Andrew L. Russell
- [Race After Technology: Abolitionist Tools for the New Jim Code](https://bookshop.org/books/race-after-technology-abolitionist-tools-for-the-new-jim-code/9781509526406), Ruha Benjamin
- [What Tech Calls Thinking: An Inquiry into the Intellectual Bedrock of Silicon Valley](https://bookshop.org/books/what-tech-calls-thinking-an-inquiry-into-the-intellectual-bedrock-of-silicon-valley/9780374538644), Adrian Daub


Movies
-------
- [Blood Quantum](https://reelgood.com/movie/blood-quantum-2019)
- [David Byrne's American Utopia](https://reelgood.com/movie/david-byrnes-american-utopia-2020)
- [The Old Guard](https://reelgood.com/movie/the-old-guard-2020)


TV Series
----------
- [Pen15, Season 2](https://www.themoviedb.org/tv/85702-pen15/season/2), Hulu
- [What We Do in the Shadows, Season 2](https://www.themoviedb.org/tv/83631-what-we-do-in-the-shadows/season/2), FX/Hulu
- [The Wire, Season 1](https://www.themoviedb.org/tv/1438-the-wire/season/1), HBO
- [Year of the Rabbit, Season 1](https://www.themoviedb.org/tv/90184-year-of-the-rabbit/season/1), Channel 4/IFC


Limited Series
---------------
- [The Good Lord Bird](https://www.themoviedb.org/tv/87523-good-lord-bird), Showtime
- [The New Pope](https://www.themoviedb.org/tv/88408-the-new-pope), HBO
- [Watchmen](https://www.themoviedb.org/tv/79788-watchmen), HBO


Podcasts
---------
- [Stay F. Homekins](https://www.stitcher.com/show/stay-f-homekins-with-janie-haddad-tompkins-paul-f-tompkins)
- [Still Processing](https://www.nytimes.com/column/still-processing-podcast)
- [You Must Remember This](https://www.youmustrememberthispodcast.com/) (*Make Me Over* and *Polly Platt, the Invisible Woman* series)
