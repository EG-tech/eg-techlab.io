---
title: "Best Things of 2021"
date: 2021-12-21
---

Comparing this year's recommendations to last year's, it's obvious that at least one difference for me between 2021 and 2020 was an ability to engage more significantly with new (to me) books, TV, and movies, including a few actually challenging works, rather than just slapping the same reruns and absolute trash nonsense on the screen to numb out. Combine that with pandemic levels of homebody time, and my total consumption of good stuff rose proportionally.

One thing I've suddenly hit a wall with is tech-related reading. While I have a bunch of [reading list](/reading-list) titles sitting on my tablet, a piece of my ongoing professional existential crisis is that I can barely stand thinking about computers outside of work hours anymore. So deep dives and recommendations from the Janet Abbate and Lisa Nakamura back-catalog will have to wait until I've worked through at least five more thrillers.

Fiction
---------
- [A Burning](https://bookshop.org/books/a-burning/9780525658696), Megha Majumdar
- [Children of the Arbat](https://bookshop.org/books/deti-arbata-9785998940194/9785998940194), Anatloii Rybakov
- [A Children's Bible](https://bookshop.org/books/a-children-s-bible/9781324005032), Lydia Millet
- [The Dispossessed](https://bookshop.org/books/the-dispossessed-9780060512750/9780061054884), Ursula K. Le Guin
- [Hamnet](https://bookshop.org/books/hamnet-0fcffd36-0ecb-4a6d-a913-4efbc6a4d6ef/9780525657606), Maggie O'Farrell
- [Interior Chinatown](https://bookshop.org/books/interior-chinatown-9780307948472/9780307948472), Charles Yu
- [Matrix](https://bookshop.org/books/matrix-9780593459652/9781594634499), Lauren Groff
- [The Noise of Time](https://bookshop.org/books/the-noise-of-time-9781101947241/9781101971185), Julian Barnes
- [No One Is Talking About This](https://bookshop.org/books/no-one-is-talking-about-this/9780593189580), Patricia Lockwood
- [Notes from a Black Woman's Diary](https://bookshop.org/books/notes-from-a-black-woman-s-diary-selected-works-of-kathleen-collins/9780062800954), Kathleen Collins
- [The Only Good Indians](https://bookshop.org/books/the-only-good-indians/9781982136468), Stephen Graham Jones
- [Piranesi](https://bookshop.org/books/piranesi-9781432886578/9781635575637), Susanna Clarke
- [Terminal Boredom: Stories](https://bookshop.org/books/terminal-boredom-stories/9781788739887), Izumi Suzuki
- [Transcendent Kingdom](https://bookshop.org/books/transcendent-kingdom/9781984899767), Yaa Gyasi


Non-Fiction
------------
- [Braiding Sweetgrass: Indigenous Wisdom, Scientific Knowledge, and the Teachings of Plants](https://bookshop.org/books/braiding-sweetgrass-3e12996d-ea04-4dd2-b9a9-04cfd82f361f/9781571313560), Robin Wall Kimmerer
- [Breaking Things At Work: The Luddites Were Right About Why You Hate Your Job](https://bookshop.org/books/breaking-things-at-work-the-luddites-are-right-about-why-you-hate-your-job/9781786636775), Gavin Mueller
- [Laziness Does Not Exist](https://bookshop.org/books/laziness-does-not-exist/9781982140106), Devon Price
- [The Secret to Superhuman Strength](https://bookshop.org/books/the-secret-to-superhuman-strength/9780544387652), Alison Bechdel


Movies
-------
- [Belfast](https://www.themoviedb.org/movie/777270-belfast)
- [Judas and the Black Messiah](https://www.themoviedb.org/movie/583406-judas-and-the-black-messiah)
- [The Harder They Fall](https://www.themoviedb.org/movie/618162-the-harder-they-fall)
- [No Sudden Move](https://www.themoviedb.org/movie/649409-no-sudden-move)
- [Summer of Soul (...or, When the Revolution Could Not Be Televised)](https://www.themoviedb.org/movie/776527-summer-of-soul-or-when-the-revolution-could-not-be-televised)


TV Series
----------
- [Back](https://www.themoviedb.org/tv/73781-back), Seasons 1-2 (Channel 4/Sundance Now)
- [Dark](https://www.themoviedb.org/tv/70523-dark), Season 3 (Netflix)
- [Pen15](https://www.themoviedb.org/tv/85702-pen15), Season 2.5 or 3 or whatever (Hulu)
- [Reservation Dogs](https://www.themoviedb.org/tv/95215-reservation-dogs), Season 1 (FX on Hulu)
- [Rutherford Falls](https://www.themoviedb.org/tv/93748-rutherford-falls), Season 1 (Peacock Premium)
- [Search Party](https://www.themoviedb.org/tv/67697-search-party), Seasons 1-4 (HBO Max)
- [What We Do in the Shadows](https://www.themoviedb.org/tv/83631-what-we-do-in-the-shadows), Season 3 (FX on Hulu)


Limited Series
---------------
- [Crashing](https://www.themoviedb.org/tv/65251-crashing) (Channel 4/Netflix)
- [I May Destroy You](https://www.themoviedb.org/tv/102619-i-may-destroy-you) (HBO)
- [The Little Drummer Girl](https://www.themoviedb.org/tv/76887-the-little-drummer-girl) (BBC One/AMC)
- [Sasquatch](https://www.themoviedb.org/tv/118556-sasquatch) (Hulu)
- [Top of the Lake and Top of the Lake: China Girl](https://www.themoviedb.org/tv/46638-top-of-the-lake) (BBC Two/Sundance TV)


Podcasts
---------
- [Black Girl Film Club](https://soundcloud.com/blackgirlfilmclub)
- [Blank Check](https://audioboom.com/channel/Blank-Check) (particularly *The Poddle Mercast* (Musker & Clements) and *The Podbreak Cast* (Elaine May) series)
- [Make My Day with Josh Gondelman](https://www.theradiopoint.com/make-my-day)
