---
title: "Macroblogging"
date: 2020-11-09
---

Did you know that social media sites like Twitter and Instagram are sometimes called "microblogging" platforms? I like the idea of modularity and microservices and breaking down my life and work and thoughts into digestible pieces as much as the next person. But I've tried to step back and reckon recently with the fact that there is absolutely nothing "micro" about my relationship with social media and with Twitter in particular.

The addictive designs used by these platforms have done their job. I constantly refresh and scroll, as if something new will have appeared in the past half-second that will better occupy my time. I follow and mute and sculpt my timeline so that I don't lose track of "the people I *really* care about" - you know, the ones that I also haven't called or texted or made any effort to check in on for a solid year *before* there was a pandemic. I laugh and share and favorite and donate, validating the time spent riddled with anxiety and boredom.

(I do **not** click on a single goddamn ad, so I guess that is one way Twitter is still **not** good at its job. Can't wait to read [this one](https://bookshop.org/books/subprime-attention-crisis-advertising-and-the-time-bomb-at-the-heart-of-the-internet/9780374538651).)

And yes, this all came to a head the week of the 2020 election. I knew well before Nov. 3rd that Twitter and Instagram/Facebook are awful, awful companies warping our social infrastructure for the profit of their executives and shareholders. I knew that my excuses for staying on Twitter ("but it's a useful professional networking tool") are pretty flimsy compared to the truth that I get a dopamine hit when people hit retweet on a Patch Bay post or my funny one-liner or whatever. I knew I should support local independent journalism, open source software, and... my friends?!?

But there's nothing like a gut-churning week of little sleep (and *still not actually talking to your friends*) to inspire you to make some changes. And one of those changes for me is going to be to value my collective time more and try to shift focus to the macro. Less time dying the death of a thousand Tweets, more time spent on intentional writing, intentional communication, intentional reading.

I admit that, again despite knowing better, I can be pulled in by fun new gadgets or trendy solutions as much as the next well-off tinkering white hobbyist. So far this personal effort has already played out, as many of my resolutions do, in the adoption of new digital solutions that I *hope* will have a macro impact:

- My [reMarkable 2](https://remarkable.com/) tablet arrived, and it may just be another *kind* of screen, but it is at least not one that burns my eyeballs and actively invites me to do something else while trying to read a great [ebook about policing](https://www.versobooks.com/books/2817-the-end-of-policing) or fill out a crossword puzzle

- I got a [HEY](https://hey.com/) email account and after a lot of fiddling have migrated over the collected communications and logins of **three** personal Gmail accounts, narrowing things down to one email for work and one for anything and everything else. (HEY might be worth its own post at some point, but my first impressions are very good)

- After backing up my archive to my NextCloud instance, I deleted my entire past roster of Tweets, deleted the app from my phone, and am in another round of aggressive muting. This has nothing to do with how other people use their accounts and everything to do with how and when I use it - I'll keep the timeline rolling during work hours, because other than the hellscape of professional listservs, it's the only place I've found to reliably find cool projects, writing, and resources to inform my digipres work. (I flirted with deleting the account entirely, and may do when EaaSI gets an official support account that I will presumably control. But I do still field a number of great questions and legitimate support requests via Twitter, and besides, *do you know how long old Linux operating systems take to install in emulation sometimes*?)

- I added this blog to this site! And I'm writing for it! Right now! Since I still very much have [The Patch Bay](https://patchbay.tech) to cover technical and digipres writing, and [The Best Films of Our Lives](https://bestfilmsofourlives.blog) available if I ever actually watch a new movie ever again, I'm not sure what this will be the space for, and that's actually kind of exciting. Poems and photos about my cat? Reviews of burritos in the Amherst area? Time will tell.

I have no idea how well all of this will stick, but already in the last couple weeks I've found myself drifting towards activities that make me feel a bit better about, y'know, being alive: actually taking steps to organize a book club with family and friends. Signing up for letter-writing and text campaigns in Georgia. Jigsaw puzzles. Crossword puzzles. It turns out I really like puzzles!

I want my turn away from microblogging not to be about being more *productive*. I will still happily "waste time" here on this blog or puttering in the backyard or doodling on this reMarkable (at least until the novlety of e-ink wears off and I remember I can't draw). I just want to feel a bit more like my time is my own again.
