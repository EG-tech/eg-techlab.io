---
title: "2023 Year in Review"
date: 2023-12-22
---

At Work
--------

I wrote quite a bit [last year](https://ethan-gates.com/2022/12/2022-personal-review/) about feeling stuck in a holding pattern at work, and boy did that feeling continue for quite a lot of 2023, until...August?

The pace of academic staff hiring (and at Yale specifically) shouldn't shock me at this point - it was about six months between when I initially applied to my current job and my eventual start date back in 2018 - but *over nine months* is a wild amount of time to be lacking a program manager/supervisor on a two-year grant. Then compound that, of course, with the semi-surprise departure of our founding, long-time department head (not wholly unexpected TBH, but the timing was A Lot), and a few other shakeups from Yale's lingering special collections reorg. That's a lot of bottlenecks, a lot of waiting for feedback, consultation and/or direction from future parties unknown, and a lot of sitting in uncertainty beyond what I'd even been used to from the term-limited grant employment dance.

But in many ways, the floodgates opened around September and it's really been an utter sprint since then. It feels like I looked up from my laptop for the first time today and three months had passed, such has been the rush of documentation, planning calls, onboarding and training, etc. I'm utterly exhausted but at the same time do feel like it's finally *towards* something (something I hope I can start to speak to in a lot more detail not long into the new year!) 

And the most assuring thing, through all the staffing changes this year that shook up who I'd be spending most of my day-to-day interacting with, was that thanks to the overall collegiality (and, less fortunately, competition) of the digital preservation field, the odds were stacked high that in the end we'd just wind up with a *different* group of fabulous co-workers. And that has been proven overwhelmingly true in these chaotic past couple of months. Despite everything, I know that I'm in very rare circumstances to be part of a *team* of passionate, expert practitioners. Too much of our profession involves toiling alone against ignorance (benign or not) and austerity, and I try not to lose sight of that perspective.

In terms of concrete/notable accomplishments, I was proud to continue presenting publicly about my work (about my work on [Qiwi](https://ethan-gates.com/2023/03/bitcurator2023/) at the BitCurator Forum, about the [potential of remote emulation](https://ethan-gates.com/2023/06/eabcc2023/) in email archiving at the Email Archiving Symposium, about the [difficulties](https://ethan-gates.com/2023/05/isea2023/) of disk imaging and emulating certain Mac hardware for the International Symposium on Electronic Art's Third Summit on New Media Art Archiving). I was happy to advise another NYU student on their graduate thesis for the Moving Image Archiving and Preservation program (particularly since this time I was drawing on my non-emulation expertise and enthusiasm for Soviet film history, which is a muscle I have not had a good excuse to flex for a while). And I was thrilled to pour a bunch of effort (possibly more than MPOW is comfortable with, but lol that is a whole other conversation) into the Software Preservation Network's petition(s) for expanded exemptions to the DMCA related to software preservation in the latest tirennial rulemaking cycle. The latter in particular is not the type of effort I would have even dreamed I would contribute to when I first started dipping a toe into archiving, so I will be particularly tickled if those petitions are successful and all this wavering and anxiety about my work life these past few years somehow got converted into structural legal change.


At Home
---------

Meanwhile...

![New house](/images/new_house.jpg)
![Penny, a dog](/images/Penny1.jpg)
![Still Penny](/images/Penny2.jpg)

New digs, new dog!

It definitely was (and felt like!) a big year of growth for Carrie and me and our little family unit. Home ownership was an enormous and nerve-wracking step, and we toiled a lot over millennial anxieties: balancing house payments with student loans (so very fun to have those back in my life, thanks NYU/Biden/Congress/Mohela/everyone else responsible for this dumb and f-ed up system!), precarious employment, family expectations. But in the end it has been so very worth it (and at least thus far, manageable) - despite the leaking ceilings and rusting pipes and uncommunicative contractors and weird scammy mail - to have this space that is *ours*, that we can make cozy and comfortable and stylish and just, an actual home. It has been a joy to continually putter and tinker and fix up and improve each weekend since we moved in May, and to be able to properly share this little haven with loved ones passing through.

And of course the biggest improvement we've made to the house was to put Penny in it! I've never had a dog before - my family were always hardcore cat people - so I was again a bit nervous, but, I mean, just look at those hound-dog eyes. She is a Chaos Muppet for sure, and it's probably going to be some years of training and aging yet before we really settle into a groove, but she is just an extra bundle of sweetness and light and energy in my life (not to mention, keeping me in my best shape ever from all the walkies!)

(And we sure have needed that little extra bit of light from time to time this year. Besides work anxieties for both of us, we've continued to struggle with the fallout of some extremely poor Decisions made by my father-in-law, who managed to dig himself into a fun, severe new legal hole even as his health continues to improve.)

I had kind of drifted socially the last couple years, honestly thanks not just to the pandemic but also the Proustian travails of returning to live in my college town and the nomadic loner-ism that settled in after uprooting and moving states and/or social circles every 2-3 years or so for the past decade. (Why work on new friendships if we were maybe just going to move back to NYC, or to Baltimore, or to Maine, or to California?) So I'm glad, in the wake of the decision to settle down and buy the house here, that I got myself out there and have found a couple of communities that I've loved getting involved with: first the Far Out Film club at the Northampton public library (which has introduced me to both some wonderful people and wild movies - definitely check out Bruno Dumont's *Li'l Quinquin* and *CoinCoin and the Extra-Humans* if you miss *Twin Peaks*), and then the [Boswords](https://www.boswords.org/) competitive crossword scene. I missed out on the latter's in-person tournament in the summer, but enjoyed lurking on the weekly live-streams and just gradually getting indoctrinated into the lingo and customs of this sub-culture (via what appears to be, dare I say, a slightly fresher and more inclusive venue for the hobby than the um, "halowed halls" of the Stamford Marriott and the American Crossword Puzzle Tournament).

(My reflections and growing thoughts on "cruciverablism" are probably worth a whole other blog post, though I simultaneously feel myself holding back because then would I become *a crossword blogger*, which is paradoxically **a thing** in the crossword world that I don't feel ready to embrace?)

Stray thoughts:
- Carrie and her mom were in a krewe parade together in New Orleans for Mardi Gras, and while I can't say that I'm likely to be won over by the Mardi Gras experience in particular, New Orleans continues to be one of my favorite places to visit and that trip was a bright spot in the February doldrums (the food, THE FOOD you guys)
- Also, it was terrific to get back down in person to NYC a couple of times (and for the first time since early 2020), both to see good friends and to remember that, despite the think-pieces, New York very much remains itself (which is to say, a wildly diffuse and unnammeable thing that shifts for each and every person that passes through it)
- I saw both Ilana Glazer and *How Did This Get Made?* live this year, probably confirming that my ability to actually absorb new pop culture tapped out around 2015.

Tomorrow: media recommendations!