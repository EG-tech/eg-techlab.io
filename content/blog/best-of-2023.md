---
title: "Best Things of 2023"
date: 2023-12-26
---

These are all things that I enjoyed this year! If you have not enjoyed them yet, I hope you do!

Addendum: if you want the full-full rundown on the year in film, do check out the 2023 "EMOs" on my other blog: https://bestfilmsofourlives.blog/2024/03/10/the-2023-emos/

Fiction
---------
- [Bad Cree](https://bookshop.org/p/books/bad-cree-jessica-johns/18965016?ean=9780385548694), Jessica Johns
- [Best of Friends](https://bookshop.org/p/books/best-of-friends-kamila-shamsie/18578441?ean=9780593421826), Kamila Shamsie
- [Don't Fear the Reaper](https://bookshop.org/p/books/don-t-fear-the-reaper-stephen-graham-jones/19777388?ean=9781982186593), Stephen Graham Jones
- [Drive Your Plow Over the Bones of the Dead](https://bookshop.org/p/books/drive-your-plow-over-the-bones-of-the-dead-olga-tokarczuk/14950549?ean=9780525541349), Olga Tocarczuk
- [Now Is Not the Time to Panic](https://bookshop.org/p/books/now-is-not-the-time-to-panic-kevin-wilson/18542533?ean=9780062913500), Kevin Wilson
- [Tomorrow, and Tomorrow, and Tomorrow](https://bookshop.org/p/books/tomorrow-and-tomorrow-and-tomorrow-gabrielle-zevin/17502475?ean=9780593321201), Gabrielle Zevin


Non-Fiction
------------
- [The Apple II Age: How the Computer Became Personal](https://bookshop.org/p/books/the-apple-ii-age-how-the-computer-became-personal/18924797?ean=9780226816524), Laine Nooney
- [Killers of the Flower Moon: The Osage Murders and the Birth of the FBI](https://bookshop.org/p/books/killers-of-the-flower-moon-the-osage-murders-and-the-birth-of-the-fbi-david-grann/533951?ean=9780307742483), David Grann
- [The Modem World: A Prehistory of Social Media](https://bookshop.org/p/books/the-modem-world-a-prehistory-of-social-media-kevin-driscoll/17978451?ean=9780300248142), Kevin Driscoll
- [Muppets in Moscow: The Unexpected Crazy True Story of Making Sesame Street in Moscow](https://bookshop.org/p/books/muppets-in-moscow-the-unexpected-crazy-true-story-of-making-sesame-street-in-russia-natasha-lance-rogoff/18254686?ean=9781538161289), Natasha Lance Rogoff
- [Run Towards the Danger: Confrontations with a Body of Memory](https://bookshop.org/p/books/run-towards-the-danger-confrontations-with-a-body-of-memory-sarah-polley/17157898?ean=9780593300350), Sarah Polley


Movies
-------
- [Asteroid City](https://www.themoviedb.org/movie/747188-asteroid-city)
- [Barbie](https://www.themoviedb.org/movie/346698-barbie)
- [How to Blow Up a Pipeline](https://www.themoviedb.org/movie/1008048-how-to-blow-up-a-pipeline)
- [The Killer](https://www.themoviedb.org/movie/800158-the-killer)
- [Showing Up](https://www.themoviedb.org/movie/790416-showing-up)


TV Series
----------
- [I Think You Should Leave](https://www.themoviedb.org/tv/88728-i-think-you-should-leave-with-tim-robinson/season/3), season 3
- [Poker Face](https://www.themoviedb.org/tv/120998-poker-face/season/1), season 1
- [Reservation Dogs](https://www.themoviedb.org/tv/95215-reservation-dogs/season/3), season 3
- [The Righteous Gemstones](https://www.themoviedb.org/tv/82782-the-righteous-gemstones/season/3), season 3
- [Slow Horses](https://www.themoviedb.org/tv/95480-slow-horses/season/3), season 3
- [Taskmaster](https://www.themoviedb.org/tv/63404-taskmaster), seasons 1-5 (thus far)


Limited Series
---------------
- [The Fall of the House of Usher](https://www.themoviedb.org/tv/157065-the-fall-of-the-house-of-usher)
- [A Spy Among Friends](https://www.themoviedb.org/search?query=a+spy+among+friends)


Podcasts
---------
- [Bonanas for Bonanza](https://podcasts.apple.com/us/podcast/bonanas-for-bonanza/id1506246892)
- [If Books Could Kill](https://www.patreon.com/IfBooksPod)
- [The Neighborhood Listen](https://podcasts.apple.com/us/podcast/the-neighborhood-listen/id1517151870), season 5
- [This Had Oscar Buzz](https://fightinginthewarroom.com/THOB/)


Games
------
- [Movie Grid](https://moviegrid.io/)
- [Wandersong](https://wanderso.ng/)
- do you really need me to link you to Tears of the Kingdom?