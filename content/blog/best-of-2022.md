---
title: "Best Things of 2022"
date: 2022-12-22
---

While I'm definitely going to continue with this rundown of all my favorite things read, watched, played and listened to in the past year, a definite trend I have to shout out: the movies are back, baby! And not just at [AMC Theatres](https://www.youtube.com/watch?v=KiEeIxZJ9x0) - I mean I rediscovered my personal love of (and attention span for) feature films in a way I haven't embraced since, if we're being honest, even before the pandemic. (I'm fairly certain I crossed over 30 new releases and over 100 new-to-me watches for the first time since at least 2017)

So while my tippy-top best movies of the year so far are presented here in non-judgmental alphabetical order, the EMOs are going to return with a true vengeance early in 2023, and I also want to write up some of my thoughts from year one of a Mubi subscription, whose one-movie-added-per-day and clear time limits set up some stakes and boundaries around cinephilia in the great morass of streaming that I found extremely rewarding. More to come over at The Best Films of Our Lives if that's of interest.

Away we go,

Fiction
---------
- [Bellwether](https://bookshop.org/p/books/bellwether-connie-willis/10112867?ean=9780553562965), Connie Willis
- [Harlem Shuffle](https://bookshop.org/p/books/harlem-shuffle-colson-whitehead/16311673?ean=9780525567271), Colson Whitehead
- [Himself](https://bookshop.org/p/books/himself-jess-kidd/6769281?ean=9781501145186), Jess Kidd
- [Klara and the Sun](https://bookshop.org/p/books/klara-and-the-sun-kazuo-ishiguro/18514778?ean=9780593311295), Kazuo Ishiguro
- [Lote](https://bookshop.org/p/books/lote-shola-von-reinhold/17687225?ean=9781478018728), Shola von Reinhold
- [My Life as a Fake](https://bookshop.org/p/books/my-life-as-a-fake-peter-carey/7825791?ean=9781400030880), Peter Carey
- [Silverview](https://bookshop.org/p/books/silverview-john-le-carre/17123826?ean=9780593490594), John Le Carré
- [Smilla's Sense of Snow](https://bookshop.org/p/books/smilla-s-sense-of-snow-peter-hoeg/7454793?ean=9781250002556), Peter Høeg
- [The Talented Mr. Ripley](https://bookshop.org/p/books/the-talented-mr-ripley-patricia-highsmith/7434241?ean=9780393332148), Patricia Highsmith


Non-Fiction
------------
- [Camera Man: Buster Keaton, the Dawn of Cinema, and the Invention of the Twentieth Century](https://bookshop.org/p/books/camera-man-buster-keaton-the-dawn-of-cinema-and-the-invention-of-the-twentieth-century-dana-stevens/18573750?ean=9781501134197), Dana Stevens
- [Living Pictures](https://bookshop.org/p/books/living-pictures-polina-barskova/17172723?ean=9781681376592), Polina Barskova (trans. Catherine Ciepela)
- [Mike Nichols: A Life](https://bookshop.org/p/books/mike-nichols-a-life-mark-harris/14711354?ean=9780399562266), Mark Harris


Movies
-------
- [Benediction](https://www.themoviedb.org/movie/399178-benediction)
- [Decision to Leave](https://www.themoviedb.org/movie/705996)
- [Nope](https://www.themoviedb.org/movie/762504-nope)
- [RRR](https://www.themoviedb.org/movie/579974-rrr)
- [Stars at Noon](https://www.themoviedb.org/movie/603204-stars-at-noon)
- [Tár](https://www.themoviedb.org/movie/817758-tar)


TV Series
----------
- [Barry](https://www.themoviedb.org/tv/73107-barry), Season 3 (HBO)
- [Cheer](https://www.themoviedb.org/tv/97401-cheer), Season 2 (Netflix)
- [Detroiters](https://www.themoviedb.org/tv/69866-detroiters), Seasons 1-2 (Comedy Central)
- [Girls5Eva](https://www.themoviedb.org/tv/100350-girls5eva), Season 2 (Peacock)
- [Reservation Dogs](https://www.themoviedb.org/tv/95215-reservation-dogs), Season 2 (Hulu)
- [Rutherford Falls](https://www.themoviedb.org/tv/93748-rutherford-falls), Season 2 (Peacock)
- [Search Party](https://www.themoviedb.org/tv/67697-search-party), Season 5 (HBO Max)
- [Severance](https://www.themoviedb.org/tv/95396-severance), Season 1 (Apple TV+)
- [Slow Horses](https://www.themoviedb.org/tv/95480-slow-horses), Seasons 1-2 (Apple TV+)


Limited Series
---------------
- [Angels in America](https://www.themoviedb.org/tv/11245-angels-in-america) (HBO)
- [The Staircase](https://www.themoviedb.org/tv/122196-the-staircase) (HBO Max)


Podcasts
---------
- [How to Survive with Danielle & Kristine](https://www.podhowtosurvive.com/)
- [Spontaneanation](https://www.earwolf.com/show/spontaneanation-with-paul-f-tompkins/)
- [Three Bean Salad](https://linktr.ee/threebeansalad)


Games
------
- [Knotwords](https://playknotwords.com/)
- [Night in the Woods](http://www.nightinthewoods.com/)
- [Return of the Obra Dinn](https://obradinn.com/)
- [Spiritfarer](https://thunderlotusgames.com/spiritfarer/)
- [Wargroove](https://wargroove.com/)

(catching up in my first year of Switch ownership I also can say that yes, Breath of the Wild, Super Mario Odyssey, Animal Crossing and Metroid Dread are very good; but both you and Nintendo don't really need me to plug those)
