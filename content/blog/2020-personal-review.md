---
title: "2020 Year in Review"
date: 2020-12-14
---

Every year I see Ashley Blewer write her ["annual report"](https://bits.ashleyblewer.com/blog/2020/12/13/twenty-twenty-annual-report-and-twenty-twenty-one-goals/) and think, that's a great idea - I, too, often feel burned out from blending endless personal and professional projects and forget to reward myself for the many things I *do* accomplish!

And obviously, this year just augmented all the latent feelings of stasis, anxiety, burn-out, etc - even though, once again, geez, when *just surviving* in this deeply broken country and world is an accomplishment, that turns every other identifiable task completed into a Nobel portfolio.

At Work
--------

**Q1**

Looking back at it, I went *hard* in the first two months of 2020, which...obviously didn't bode well for the rest of the year. January was spent haunting [METRO](https://metro.org/) (RIP to 599 11th Ave and your goddamn elevator), co-teaching a workshop on emulation with Claire Fox. Eleven months later, this is still one of my favorite things from the year - emulators are *hard* and *finicky* and teaching them hands-on in a compressed period of time, with twelve people all encountering different problems, is nigh impossible, and I'm so grateful and proud of what Claire and I cooked up, and hope we can run it again someday. I tried to make a Carpentries-style online [walkthrough version](https://eaasi.gitlab.io/program_docs/intro-emulation-workshop/) of the exercises we did, and would be super interested if it's of use to people.

I also spoke with Limor Peer about emulation and reproducible research at Yale for the [Librarians Building Momentum for Reproducibility](https://vickysteeves.gitlab.io/librarians-reproducibility/) event, which did collaborative, accessible online conferencing first and better than anyone else in 2020.

In February I was sent to Dublin for the International Digital Curation Conference, co-leading a day-long workshop with my Software Preservation Network friends Tracy Popp and Lauren Work (shout-out also to Monique Lassere and Jessica Meyerson for the *very extensive* pre-trip prep that we could not have pulled it off without). That was a super grueling day and I honestly spent most of the rest of the conference recovering in a jet-lagged haze, but bookmarked a lot of people's projects/names while running straight to the local pubs after every session, alone (look, I didn't know it would be the last chance I had to socialize in-person). But Dublin was beautiful, cold, and I hope that the direct flight from Hartford is still around when vacationing is something we can do again so I can go back and broodily stand on Howth Head again.

OK, and then we all get sent home for work in early March, and this is the part that I haven't ever really talked about. At this point we were already prepping for me to begin a "permanent" (you know, as far as grant-funded term positions allow) WFH arranagement in late May - so moving that up a couple months wasn't actually very disruptive or honestly, all that stressful. What **was** stressful was that, right as a goddamn pandemic started, my supervisor went on paternity leave, *his* supervisor got stuck in New Zealand, and I got stuck holding the project management reigns on a scheduled release of the EaaSI platform, which is a complicated task of coordinating two development teams and solely running QA/QC on an expansive and open-ended platform in the best of times.

It didn't go according to plan, and the "2020.03" release devolved into a (I'll say it) buggy and maybe 75%-baked 2020.05 release that I pushed out the minute I stopped *actively* getting error messages every ~5 seconds, because I couldn't take it any more. Everything's fine (because look emulation is cool but not THAT big a deal), and I'm grateful to the rest of the EaaSI team, my bosses included, being generally understanding, empathetic, and extremely flexible people, but boy did I hate every minute of that. Project management/administration is not my thing and it was...revealing to really discover that about myself under the flame.

**And the rest**

Everything after that release went out in May is a big old blur, but I know a bunch still happened. We transitioned into the second phase of our grant funding in the summer, and I've technically taken over as "User Support Lead" for the EaaSI project as a result. Objectively speaking that doesn't mean too much day-to-day except that I have more time to do things like make [Training Modules](https://www.softwarepreservationnetwork.org/eaasi-training-module-emulation-environments/), work on Case Studies, plan upcoming roundtable discussions and build a public Discourse forum for emulation and software preservation. I'm super excited to share that last one in particular in late January or February :)

We did some hacking on emulation and disk imaging at the BitCurator User Forum - Hannah Wang was so amazingly supportive of my hare-brained brainstorming and actually turning that into an event with, like, breakout rooms and registration emails and all the other stuff you actually need to do to host a virtual session.

I also helped with EaaSI presentations or panels at the Virtual DLF Forum and the Fall CNI Meeting. There are recordings somewhere. I'm too exhausted to find them again.

**AMIA CEA TF**

At the beginning of the year I was invited to participate in the Association of Moving Image Archivists' Continuing Education Advocacy Task Force. I haven't been involved with AMIA for several years and have mixed feelings about that org's reliance on volunteerism, among other things, and broader digital preservation communities have been more relevant to my work lately. But I liked the idea behind the Task Force, and it seemed a great opportunity to bring my perspective and digipres knowledge and support back to a group who, certainly, did a lot for me coming up through grad school and starting as an archivist/technician/whatever I am. I think training programs and freely available continuing ed materials are the obvious and critical place to build a more inclusive archival community.

But - turns out this was not the year for me to try to solve my problems with audiovisual archival education and professional/labor organization, like as a side-thing. I'm very grateful to Brianna Toth, Teague Schneiter, Andy Uhrich, Nicole Martin, and everyone else on the Task Force for putting up with me disappearing for weeks at a time because *I just couldn't fill out another Doodle poll*, and I can't believe what they've put together during, again **an actual global pandemic**. I'm encouraged how much of the programming the TF has sponsored has been about archival labor, that the sliding-scale registrations for one-off events were super feasible, that figuring out how to compensate instructors became normal talk, all things that felt very different from my AMIA participation only a few years ago. But I had to start saying no to things to preserve my own sanity, and I chopped this out around August.

Working with the TF was another personally-revealing stress point. I **love** leading workshops, coming up with lesson plans and training resources, fielding questions on technical topics from students and colleagues. Give me a time and a place and a topic and I'll be there. I *receive* energy from it. Ask me to coordinate the time and the place and the topic with five other folks in Yet Another Collaborative Workflow Tool - on my own time - I'm going to melt down.

I feel bad about this. Facilitation and planning is *so much work* and it's unfair to always put that on other people so that I can prance in and be the super-visible one who knows what a math co-processor is or whatever. But again, I can't (and no one should) be doing that shit for free anymore. Unless it's on my own blog...

**The Patch Bay**

I am super happy that [The Patch Bay](https://patchbay.tech) got a bit revived this year after no new posts for ~2 years. Thanks again to Jeff Lauber for contributing a super clean and concise paper about Signal to Noise Ratio - I'd still love to expand the Patch Bay more as an editing/collaborative project if anyone's interested in contributing. I also managed to puke out a long-gestating piece about package managers. This stuff always takes way more time/research/thought than I think it will - it's a lot to do on top of normal work duties, so in the future I'm really fine if I crank out maybe one new post a year.

**This blog**

I also migrated this personal site from Jekyll to Hugo and added this blog just in the past few months. Speaking of things that take more effort than you think they will.


At Home
---------

**Terminer**

Also in February (how DID I survive this year) we went to Pittsburgh to see my sibiling Philip's incredible show [*Terminer*](https://newhazletttheater.org/events/terminer/). I haven't been able to see much of Philip's work since they left New York so I was so glad to have this chance - and uhhhhh a cyber-queer piece about sitting in uncertainty with violence, history, our own bodies/selves, and the future was ON POINT for 2020 slash I'm not entirely unconvinced that the coven didn't accidentally summon something that night but never mind.

Philip's moved to Philadelphia now and I hope them being that much closer means I get to see more of their stuff...when theater exists again!

**Moving**

"Home" is both different and, funnily, the same again. Carrie got a new job with Five Colleges Inc. in March (or June, depending on who you ask because lololol grant funding and contracting in a pandemic) so in June we moved 45 minutes up the road from West Hartford to Amherst, MA.

It's obviously been a weird homecoming - all my favorite shops, restaurants, bars, venues from college years have obviously been off the table, and I've been terrified to visit and reconnect with friends, mentors, the Amherst community. But wowowow our day-to-day existence is so much better and I am just so profoundly grateful to have had literal space to breathe for the past seven months. I guess I won't go into my whole Connecticut rant because I still have to work with people who live there. I'll just say 1) our neighbors have a *very* good dog named Milo that we get to take on walks sometimes, 2) I've never eaten better because Carrie is a wizard and we're in like three simultaneous farm shares because *we will eat it all*, and 3) our apartment is a converted '70s studio loft with cozy wood paneling with big ol' windows looking directly over trees and mountains and it feels a bit like glamping all the time and I refuse to feel bad about saying I'm a basic white liberal who likes that.

Coyotes are a fucking thing though, did you know they *cackle* at night???

**Last but not least**

There was a good thing about Hartford this year though:

![Wedding 1](/images/wedding1.png)

![Wedding 2](/images/wedding2.png)

And I wouldn't trade that day for any other year.
