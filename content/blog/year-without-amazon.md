---
title: "A Year Without Amazon"
date:
draft: true
---

I forgot, in all the forced and unforced reflection on 2020, that it was also the year that Carrie and I decided to follow through and properly excise Amazon. I haven't been a Prime member for years (I think they were giving out free student subscriptions some time back in like...2011? Does that sound right to anyone?), and, thanks to my mother (an extremely principled librarian/bookseller who has been supporting and advocating for independent shops since Barnes & Noble and Borders were the Big Bads) was always wary of Bezos' plans. But there were certainly many times that I casually defaulted to Amazon for whatever service or goods over the past decade, and Whole Foods was regrettably the only place within ~75 miles where we could get bulk grains in Hartford.

(Again, Connecticut, I'm sorry but you are a garbage state)

So 2020 we decided to walk the walk, go through it all, and methodically cut out Amazon-owned companies. The most frequent question we got (at least, from parents and older generations who were maybe not already being indoctrinated by Instagram info-activism) was "how?", and I won't lie - it's at the same time both easier and harder than you think to cut out Amazon completely, for reasons I'll get into in this rundown of the alternative pursuits we tried. But I thought, most practically, people are generally on board with alternative options as long as it's *still convenient*, so I'm just going to throw some links and thoughts in various categories of service here.

If the fact that Jeff Bezos [basically doubled](https://www.democracynow.org/2020/9/15/pandemic_profiteering_amazon_caught_price_gouging) his already-unfathomable personal wealth during a global pandemic with untold thousands dead isn't enough to convince you of the need to reduce our collective reliance on Amazon, here are a few primers and resources. Note that, while it's good to think about where our stuff comes from and the people who actually get it to us and the impact on our local communities, we're also absolutely going to need regulation and higher levels of accountability to actually do anything beyond swap one business for another. I eagerly await THAT Big Tech anti-trust lawsuit.

- [How to Resist Amazon and Why](https://microcosmpublishing.com/catalog/zines/12043)
- ["Logistics Workers of the World: A Conversation with Agnieszka Mróz of Amazon Workers International"](https://logicmag.io/care/logistics-workers-of-the-world-a-conversation-with-agnieszka-mroz-of-amazon/)
- https://www.reddit.com/r/boycottamazon/

### Whole Foods/groceries

- [Thrive Market](https://thrivemarket.com/)
- [Misfits Market](https://www.misfitsmarket.com/) (or similar - Imperfect Produce, etc)

### Books

- [Indiebound](https://www.indiebound.org/)
- [Bookshop.org](https://bookshop.org/)
- [Better World Books](https://www.betterworldbooks.com/)
- [Biblio](https://www.biblio.com/)

### Audible

- [Libro.fm](https://libro.fm/)

### GoodReads and LibraryThing

- [The StoryGraph](https://www.thestorygraph.com/)
- [BookWyrm](https://github.com/mouse-reeve/bookwyrm)
- [Inventaire.io](https://inventaire.io/welcome)
- [Readng](https://beta.readng.co/)

### Prime Video

- [torrenting](https://www.vpnmentor.com/blog/torrent-beginners-bittorrent-explained/)

### IMDb

- [The Movie Database (TMDb)](https://www.themoviedb.org/)

### Twitch

- [Open Streaming Platform](https://openstreamingplatform.com/)
- [Owncast](https://owncast.online/)

### Electronics and Computer Shopping

- [Newegg](https://www.newegg.com/)
- [Micro Center](https://www.microcenter.com/)
- [Other World Computing/MacSales](https://eshop.macsales.com)

### Crafts

- [Etsy](https://www.etsy.com/)

### Amazon Web Services (AWS)

- [Digital Ocean](https://www.digitalocean.com/)
- [Kimsufi](https://www.kimsufi.com/us/en/index.xml)
- [Linode](https://www.linode.com/)
- [NearlyFreeSpeech.net](https://www.nearlyfreespeech.net/)
- [Netlify](https://www.netlify.com/)
- [Paperspace](https://www.paperspace.com/)
- [Reclaim Hosting](https://reclaimhosting.com/)
- [Vultr](https://www.vultr.com/)
- [Wasabi](https://wasabi.com/)

### Lastly

If you think you want a Ring camera, Amazon Alexa, or any other "smart-home" device, no you don't, stop it, what is wrong with you?
