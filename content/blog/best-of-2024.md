---
title: Best Things of 2024
date: 2024-12-29
---
Fun stuff! Fun stuff! Fun stuff!
## **Fiction**

* [The Angel of Indian Lake](https://bookshop.org/p/books/the-angel-of-indian-lake-stephen-graham-jones/20211525?ean=9781668011669), Stephen Graham Jones
* [Birnam Wood](https://bookshop.org/p/books/birnam-wood-eleanor-catton/18402449?ean=9780374110338), Eleanor Catton
* [Black Hole](https://bookshop.org/p/books/black-hole-charles-burns/18350803?ean=9780375423802), Charles Burns
* [Crook Manifesto](https://bookshop.org/p/books/crook-manifesto/18888269?ean=9780525567288), Colson Whitehead
* [The Fraud](https://bookshop.org/p/books/the-fraud-zadie-smith/19427896?ean=9780525558965), Zadie Smith
* [The Goose Fritz](https://bookshop.org/p/books/the-goose-fritz-sergei-lebedev/10609766?ean=9781939931641), Sergei Lebedev
* [Land of Milk and Honey](https://bookshop.org/p/books/land-of-milk-and-honey-c-pam-zhang/19729816?ean=9780593538241), C Pam Zhang
* [Ripley's Game](https://bookshop.org/p/books/ripley-s-game-patricia-highsmith/8837733?ean=9780393332124), Patricia Highsmith
* [The Secret Hours](https://bookshop.org/p/books/the-secret-hours-mick-herron/19426619?ean=9781641296007), Mick Herron
* [The Vaster Wilds](https://bookshop.org/p/books/the-vaster-wilds-lauren-groff/19900858?ean=9780593418406), Lauren Groff
* [The Witch Elm](https://bookshop.org/p/books/the-witch-elm-tana-french/15280729?ean=9780735224643), Tana French

## **Non-Fiction**

* [Blood in the Machine: The Origins of the Rebellion Against Big Tech](https://bookshop.org/p/books/blood-in-the-machine-the-origins-of-the-rebellion-against-big-tech-brian-merchant/17824365?ean=9780316487740), Brian Merchant
* [Image Objects: An Archaeology of Computer Graphics](https://mitpress.mit.edu/9780262045032/image-objects/), Jacob Gaboury
* [The Riddles of the Sphinx: Inheriting the Feminist History of the Crossword Puzzle](https://bookshop.org/p/books/the-riddles-of-the-sphinx-anna-shechtman/20143426?ean=9780063275478), Anna Schectman

## **Movies**

* [A Different Man](https://www.themoviedb.org/movie/989662-a-different-man)
* [I Saw the TV Glow](https://www.themoviedb.org/movie/858017-i-saw-the-tv-glow)
* [Janet Planet](https://www.themoviedb.org/movie/1037035-janet-planet)
* [La Chimera](https://www.themoviedb.org/movie/837335-la-chimera)
* [Problemista](https://www.themoviedb.org/movie/852247-problemista)

## **TV Shows**

* [The Franchise](https://www.themoviedb.org/tv/216619-the-franchise), season 1
* [Girls5Eva](https://www.themoviedb.org/tv/100350-girls5eva), season 3
* [Justified](https://www.themoviedb.org/tv/1436-justified), seasons 1-3

## **Limited Series**

* [Baby Reindeer](https://www.themoviedb.org/tv/241259-baby-reindeer)
* [Over the Garden Wall](https://www.themoviedb.org/tv/61617-over-the-garden-wall)
* [Ripley](https://www.themoviedb.org/tv/94028-ripley)
* [The Sympathizer](https://www.themoviedb.org/tv/129398)
* [True Detective: Night Country](https://www.themoviedb.org/tv/46648-true-detective/season/4)

## **Podcasts**

* [The Big Picture](https://www.theringer.com/podcasts/the-big-picture) (but only if/when Amanda Dobbins is on)
* [5-4](https://www.fivefourpod.com/)
* [Good Job, Brain!](https://www.goodjobbrain.com/)
* [The Neighborhood Listen](https://podcasts.apple.com/us/podcast/the-neighborhood-listen/id1517151870), seasons 6-7
* [System Crash](https://systemcrash.info/)

## **Games**

* [Chants of Sennaar](https://www.rundisc.io/chants-of-sennaar/)
* [Luigi's Mansion 3](https://www.nintendo.com/us/store/products/luigis-mansion-3-switch/)
* [Okami HD](https://www.okami-game.com/)
* [The Talos Principle](https://en.wikipedia.org/wiki/The_Talos_Principle)
* [Turnip Boy Commits Tax Evasion](https://en.wikipedia.org/wiki/Turnip_Boy_Commits_Tax_Evasion)
* [Wargroove 2](https://en.wikipedia.org/wiki/Wargroove_2)