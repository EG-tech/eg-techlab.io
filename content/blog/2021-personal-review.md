---
title: "2021 Year in Review"
date: 2021-12-19
---

At Work
--------

What would a year look like where I actually stopped, slowed down, or otherwise invested myself less in work? That was sort of the running question in the back of my mind throughout 2021.

I quite vividly remember the feeling of *some* rest and hope that I had on January 1st. A skin-of-our-teeth election victory ("victory"); vaccines for all on the horizon; two weeks of rest and recuperation; and, however much not seeing family and friends sucked, casting aside long-standing traditions and dynamics in favor of a completely new way of celebrating the holidays did admittedly have some thrill to it.

Then of course January 6 hit, put me right back in the hole, and somehow even more so than in 2020 I've found myself grappling this year with extended, capital-D depression for the first time in my life. Pair that with a couple of formative reading experiences early in the year - *Laziness Does Not Exist* and *Breaking Things At Work*, both of which will make an appearance shortly in my year-end recommendations - and more than ever I kept thinking in 2021 that this, this *must* be year I do less, try less, accept less.

But of course, here I am looking back and writing this post, and even a cursory summary of accomplishments shows how utterly this was not the case.

I continued this year giving talks about emulation and the EaaSI project in my usual professional circles: for METRO, for grad school students at NYU and Simmons, at a poster session at SAA. I tried out something a little different with a lightning talk about QEMU at the BitCurator User Forum, which I wound up enjoying immensely both for the brevity of format and the ability to step out of implicit "pitch" mode for a minute.

Another lovely thing that happened professionally was the publication in September of *The Handbook of Archival Practice*, featuring an entry I was invited to contribute on the potential of emulation in archive workflows (I actually wrote the thing in summer 2020, so this was a double bright spot in terms of getting a morale boost for long-past effort). It is an odd thing that even though I have tried to swing my whole professional arc to be about knowledge-sharing and creating resources for others, I remain a little taken aback any time someone actually finds something I wrote or made useful.

(When Dr. Franks first reach out asking if I was interested in contributing to the Handbook, I swear I wrote and nearly sent a response that she had the wrong person and that one of my supervisors would obviously be more qualified to write the chapter than me; a reaction that makes all the less sense when you consider both my supervisors are *also* straight white cis men who work at Yale and have spoken or been published on this subject at length, so, like, this would not exactly have been a productive gesture on my part)

I am so glad we recorded one of the most engaged and engaging conversations I've ever had about emulation in February as an [EaaSI roundtable discussion](https://www.softwarepreservationnetwork.org/eaasi-roundtable-1/) with Eric Kaltman, Tracy Popp, and Fernando Rios (I will, also, gleelfully, take credit for that title). I put out several more EaaSI ["training modules"](https://www.softwarepreservationnetwork.org/emulation-as-a-service-infrastructure/resources/#modules), which are some of my favorite things to work on. And we brought a whole bunch of new users to the EaaSI platform with our 2021 hosted emulation services pilot program with the Software Preservation Network.

So my attitude about work and digital preservation remains equivocal to the degree that I often have truly no idea which "me" is going to show up every morning to work: the one who lasers in and gets some good writing and support work done, or who finds everything I'm doing pointless in the face of overlapping global crises and struggles to send one email. Clearly, in combination, this person is doing just fine career-wise. Is it how I should be, is it what I want to be, is it the only me that I can be? Maybe! Maybe not! I truly don't know! All I can say for sure is that I know I'm grateful for good colleagues and lucky to be in the stable, safe, and intellectually challenging place that I am.

At Home
---------

Personally, I remained a shut-in until being fully vaccinated at the start of June, and on the whole I basically took the year essentially just to reconnect with family and be a bit less freaked out when going to various stores. As we head into another phase where caution and care *absolutely* must go first, on reflection I am struggling with some realization of how I let a lot of my personal and social actions be guided by fear and a natural homebody tendency even in the the relatively "better" times. To be very clear, I'm talking about even pre-pandemic times. I'm quite happy to read books, watch movies, fill out crossword puzzles and eat snacks for the foreseeable future. But I *do* enjoy other things, including other people, too! I'd like to find some more mental or actual balance in those things in 2022.

Anyway, I had a nice time exploring the corners of New England this year. Carrie and I went camping in Charlemont in May, Provincetown in June, and Otter River State Forest in August. We stayed on the Vermont side of Lake Champlain in April, Williamstown and the Berkshires for my birthday in July, and I spent a week in the White Mountains (a childhood family haunt) with my parents and sibling in June.

Unfortunately, the tail end of acceptable outdoor-adventure weather in the fall got kneecapped by the crisis-point (and more fortunately, resolution) of a long-simmering health issue for my father-in-law in September and October. The full story involves a lot of twists and turns that are not totally my place to talk about, and compounded by a domestic violence incident that I probably still shouldn't write about publicly for ongoing legal reasons! So that was...a lot, and the last couple months have been pretty much dedicated to some semblance of recovery.

(The good news is that the medical side of things played out nearly to the absolute best-case scenario, which relieves a burden my wife has had hanging over her head for over two years; the other factors at play, while frustrating and painful, are also things that we ultimately do have the power to separate from to one degree or another)

My hope for things on this end in 2022 is just that Carrie and I can finally have the space to think a bit more about where we *want* to be: our home, our habits, our shared lives. That sounds a bit scary or like, heavy emotional labor or something, but I don't mean it that way. Planning and dreaming with Carrie is one of the best and most fun things to do - it's why marriage was such an easy decision, even if the logistics around it were a bit pandemic-rushed - so when I say I want to do more of it, I guess that's a sign that I don't truly find things hopeless after all.

If you somehow read this far, thanks for sticking through a more therapeutic writing session than I'd anticipated. I'll top it off with a few, slightly less **Meaningful** highlights of the year:

- We got kayaks *and* bikes this year, so I declare 2022 to be the Year of Sore Limbs
- After pouring ~60 hours into Hollow Knight, I gave up and ended my playthrough without completing the Trial of the Fool or any of the Godhome boss battles, so I'll have have to go back once I finish some Qi quests in Stardew
- Managed to reach Queen Bee twice in the New York Times Spelling Bee game before they added the official hints section (which I now use proudly and shamelessly)
- Switched Android icon packs approximately six times, because why not?

Media recommendations for the year, soon!
