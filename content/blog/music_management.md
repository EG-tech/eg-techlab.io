---
title: "Adventures in Recreating iTunes c. 2005"
date: 2021-05-24
---

I was an early adherent/adopter of Spotify - I want to say sometime in 2011, as a student in college, I think pretty much as soon as it was available in the U.S. The convenience was incredible! No more burning mixes to CD to bring to a party! No more painstaking loading of individual songs and albums on to an iPod or smart phone, then struggling to find an auxiliary cord! No more needing to guess whether buying an album was worth it based on 30-second samples! No more needing to *buy albums*, period! (Conversely, it felt more legitimate and supportive than straight up torrenting, and didn't require endless, finicky metadata management!) I was 21 and broke and liked a ton of mainstream stuff (but like *indie* mainstream stuff), of course I loved Spotify.

I'm not going to get into the ethics stuff and how I eventually learned that Spotify is objectively terrible for musicians (and ergo, you know, music). But I'll say that I recently realized how much less music I've listened to, at all, over the course of the past decade or so - partly because podcasts took over so much of the space where I would otherwise do that (commuting, car trips, routine/mundane work tasks), but partly because as I got older the convenience proved to be a trap. The less I thought about music, the less I listened, full stop.

Nothing requires more *intention* for me than making a mix tape/CD. I'm not going to go all John-Cusack-in-High-Fidelity on you - Gen Z probably gets this way around shared Spotify playlists and TikToks, and that probably sounds disparaging but I mean it in utter sincerity. This is just about my personal taste and experience, and re-discovering the state of mind that I associate with appreciating music. So, I set out to recreate the experience of iTunes circa 2005 and burn my friends a mix, which I shall then, in all seriousness, mail them.[^footnote1]

The following are a few notes on the tools and setup I'm trying out - FOSS music management has gotten pretty fun, as it turns out!

1. **Acquiring music**

  If at all possible - buy the music you like! [Bandcamp](https://bandcamp.com/) is great, more and more artists seem to be putting stuff on there, and if you can't find musicians you know, try just trawling around - most artists let you stream/sample at least a song or two per album, if not the entire thing. Or find a friend you trust and pester them until you get some picks that interest you! That's how music recommendations work![^footnote2] It took me a while to get into Bandcamp because I was scared/uncertain about this step, but I finally just put it out there and some cool people gave me great recs and I'm hooked now. You can also check if bands you like have Patreons or fan clubs on their web site, as another way to get digital downloads. Fan clubs are more of a thing still than you think!

  If that doesn't work - or, critically, if we're talking about pop artists and labels that *truly do not operate based on what you do as a consumer either way* - have you heard the good news about [youtube-dl](https://youtube-dl.org/)?

  Basically, there's tons of music posted on YouTube. Google even reconfigured their whole music streaming service offering based on that idea (I still don't understand what YouTube Music is, and I refuse to, so don't @ me if this is wrong somehow). Correspondingly, there is a healthy ecosystem of tools to download audio from YouTube. youtube-dl and [pytube](https://pytube.io/en/latest/) are both very popular and you can use them both directly as command-line tools - or, because they're both open source, any number of people have built attractive scripts and clients on top of them to make the experience more user-friendly and streamlined for building libraries like I'm doing.
  
  {{< figure src="/images/spotdl.png" caption="*I have to start somewhere*" alt="A screenshot of the spotdl command-line tool running a download of Carly Rae Jepsen's song Run Away With Me" width="600px" >}}

  (Note - there's also a whole lot of sketchy and scammy "Download YouTube" sites and programs out there if you're just Googling. Be careful! And probably don't pay for anything, unless it's tipping/supporting an open and transparent developer who made a thing you like and find useful! I'm talking more, again, about open source software that you'll find on Github, GitLab, Sourceforge, etc. and not "MP3xYTxDownloadsxForxFree.com")

  I am personally fond of this spotify-downloader/[spotdl](https://github.com/spotDL/spotify-downloader) tool.[^footnote3] Contrary to its name, it's not downloading or decrypting anything from Spotify - it's just pulling metadata from the Spotify API, including the songs, artists, and playlists you've liked on your Spotify account, and then pulling down the actual audio from YouTube.[^footnote4] It's very handy if you've invested a decade in favoriting stuff and making half-hearted playlists on Spotify and now want to extricate yourself, and I favored a command-line tool over a GUI, for reasons I'll get into in a minute. Spotdl will also handily pull a whole bunch of Spotify's metadata (album art, track info, release year, genre tags, etc etc) as well as lyrics from Genius, and embed them into your download. So not only is this method easier than using public torrent trackers, so far I've had to do very very minimal metadata fiddling just to clean up my collection. Artist, track and album info all pretty much squares itself and works out-of-the-box after download.

  It should also be noted that, if you're a true audiophile, YouTube quality tends not to be, like, the best by default. You can tweak spotdl/youtube-dl/pytube settings to improve this, but just know you're probably never going to compete with Tidal on this point if that matters to you. (Sure there are private torrent trackers to look into for that, tho)

2. **Trimming music**

  The downside to automated downloads from YouTube is that sometimes you'll end up getting the audio from a music video or other YouTube-y artifacts rather than just the straight track. This can be fixed quickly with an audio editor - so far I've just been using [Audacity](https://www.audacityteam.org/) because of my familiarty with it, but I don't love needing the extra Export step after trimming rather than just a quick "save" on the track. I have to imagine there's a quicker/lighter tool just for trimming audio, but I haven't done the research yet here, so do send me your recs!

3. **Playing music**

  I don't really like Rhythmbox, the default music player/manager that ships with a lot of Debian-based Linux distros (it's a little *too* bare-bones), so I went looking for alternatives and lots of people recommended Clementine. However, Clementine hasn't been updated since 2016 and it seems like its advertised features (like connecting directly to your library in streaming services like Spotify) are starting to break down. So I'm trying a more actively-maintained fork called [Strawberry](https://www.strawberrymusicplayer.org/), and digging it!
  
  {{< figure src="/images/strawberry.png" alt="A screenshot of the Strawberry Music Player running on Ubuntu Desktop, open to a playlist" width="700px" >}}
  
  Strawberry's further notable on the metadata front, because if spotdl and the Spotify API don't do the trick - I dunno, if something's too obscure or whatever other reason - it can hook up to a bunch of other sites and services to fill in the gaps: Last.fm, Discogs, Qobuz, Tidal, Musixmatch, Genius, AudD, ChartLyrics...if your download is missing embdedded album art, lyrics, or even more advanced information like musician profiles, Strawberry does an excellent job of finding and reconciling all of that instantly. 
  
  There's plenty of options to get into the details and edit things if you *do* want to get fiddly, but I've been incredibly impressed at how much work music players can and will do for you now, compared to how back in the day iTunes used to inexplicably place artist info in the "Composer" field or reset your carefully, manually-curated album art willy-nilly.
  
  Otherwise, Strawberry has all the stuff you would expect: playlist creation, the ability to hook up to streaming radio stations or podcast RSS feeds, loading music onto mobile devices, connection to last.fm or libre.fm if you use those services to track, share and expand your listening habits. (I think this is called "scrobbling"? I'll be honest, I don't really understand what Last.fm is for) One thing I discovered, given my own quixotic needs, is that while it will play back from audio CDs, Strawberry does not have built-in capability to burn a new audio CD - but, I expect that is not an issue for most. (I ended up just using [Brasero](https://wiki.gnome.org/Apps/Brasero) for that, in any case, which is quick and easy)
  
  
4. **Backing up and streaming**

  At this point, spotdl and Bandcamp + Strawberry could make for a potent combo to (re)build a local music library. But I'm not satisfied because I love messing with my personal NextCloud backup server and was curious what options it might have available specifically for music/audio sync.
  
  There were two major options for NextCloud extensions/apps: NextCloud Audio Player and NextCloud Music. Audio Player is a very straightforward and lightweight extension that lets you organize and play audio files with a few more features than just straight playing .mp3s or whatever out of your Files organization (album art, playback controls, etc.) NextCloud Music, on the other hand, both turns your NextCloud web client app into a fuller-featured player (playlists, folder organization, metadata editing, etc.) AND automatically adds and configures an Ampache music streaming server on your NextCloud back-end. [Ampache](https://ampache.org/) is then also cross-compatible with [SubSonic](http://www.subsonic.org/pages/index.jsp) clients - like Strawberry!
  
  {{< figure src="/images/nextcloud_music.png" alt="A screenshot of the interface of a NextCloud storage server. The music app is open and playing 'Closer' by Tegan and Sara from a playlist." width="800px" >}}
  
  In other words, with very minimal setup (just getting an API key, which NextCloud walks you through very clearly), I can stream audio straight from my NextCloud Music library into Strawberry.[^footnote5]
  
  {{< figure src="/images/strawberry_subsonic.png" alt="A screenshot of the Strawberry Music Player again, but this time it is playing the same Tegan and Sara song from the previous picture. The song's Source field is circled to highlight that it is streaming from the NextCloud Subsonic/Ampache server" width="800px" >}}
  
  Putting it all together: with only one spotdl command and a "refresh", I can pull down music from Youtube directly into my NextCloud Music library and start listening to it on the desktop and making playlists in Strawberry, which automatically adds all the metadata bells and whistles to the stream. With a solid snapshot/backup strategy already in place for my NextCloud files, I should never even have to worry about messing with the audio files themselves locally or manually uploading.

[^footnote1]: The contents of said mix are probably their own blog post. In any case, the boundaries were that I could only include songs released post-2012 (when I graduated college) by bands/artists that were regulars on my mixes in the aughts. A couple exceptions were made for more recent artists either with a clear inspiration from the aughts (Charly Bliss, for example, makes the cut by dint of being influenced by "The Killers, The All-American Rejects, and Fountains of Wayne", their words) or, subjectively, I could easily imagine blasting out the window of my friend's tiny 2003 Honda Fit while tootling around Shaker Heights, frappuccino in hand.

[^footnote2]: Sometimes I want to listen to Sleater-Kinney, sometimes to Dan Malloy's weird unreleased off-off-off Broadway musicals! Algorithms will never really get that!

[^footnote3]: I did have some fussy Linux problems with getting spotdl working at first, because it requires ffmpeg 4.3+ and the default in Ubuntu 20.04 repositories is still 4.2.x. This is one of those times that those on macOS or Windows should have no/much less concern, or I guess if you install ffmpeg via Snap, which, eugh.

[^footnote4]: For the record, decrypting downloaded Spotify audio doesn't seem possible right now, but don't think I didn't investigate.

[^footnote5]: The only drawback so far to this situation so far is that playlists made in Strawberry or in the NextCloud Music app don't seem to sync with each other. I need to keep messing with it to see if this is an option I'm missing, but it's not the worst thing to imagine e.g. creating Work Playlists on Strawberry for work, and personal playlists for sharing in the NextCloud Music web UI, but it might get redundant. FWIW, on mobile, the [UltraSonic](https://github.com/ultrasonic/ultrasonic) client seems to pick up automatically on playlists from the NextCloud Music server, so I suspect I'm just missing something obvious in Strawberry.
