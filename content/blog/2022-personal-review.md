---
title: "2022 Year in Review"
date: 2022-12-17
---

At Work
--------

To be perfectly honest - weird year! Living on grant funding has always been a bit squiffy of course, wondering where exactly I'll be this time next year, or the next. (The answer always being: probably doing the same thing I'm doing now! But always that *_probably_*) But even in years past, even in the utter lows of the pandemic, inching forward on deliverables and projects, building up my skills, sharing expertise in documentation and presentations, I could always get some sense of building up **something**, moving forward even if not always at the pace or manner I wanted,or that funders still seemed to expect.

This year was more of a holding pattern than ever, particularly when it came to waiting for big strategic decisions that affected prioritization and planning. Waiting for our program manager to come back from leave. Waiting to get confirmation on our third round of funding. Waiting to see how a massive special collections reorg at Yale would affect our team. Waiting for Germany's internet to stop melting down from extreme heat and power shortages ("lol"). Now, waiting to re-hire a program manager.

As usual, in retrospect that feeling of stasis is also tempered by looking back at the number of things I got done. More training modules. EaaSI's first official [API docs](https://eaasi.gitlab.io/eaasi_user_handbook/api/eaas.html) (to be expanded soon!). Remote presentations for NYU's Institute of Fine Arts, Software Heritage's "[SWHAP Days](https://ethan-gates.com/2022/10/swhap_days2022/)" event, and [No Time to Wait 6](https://ethan-gates.com/2022/10/nttw6/). Co-author on an [iPres paper](https://ethan-gates.com/2022/10/ipres2022/). Support for an emerging "AusEaaSI" community. Service on the Software Preservation Network's Community Engagement Collaborative, and both BitCurator Consortium's Educators Membership Working Group and Membership Committee. Etc!

But it's been a real "work won't love you back" year, waiting on pins and needles for weeks to hear whether a disembodied administrative email signature deems those efforts worthy of further existence (or even acknowledgement). I'm trying to resolve not to let that aspect of things waste my time/brainspace again in 2023, to channel energy into more of what I'm good at: writing, training, making tools to help others with emulation.

To that end, it's been really rewarding to make [Qiwi](https://gitlab.com/EG-tech/qiwi), a quick little graphic launcher for QEMU that has been a great way for me to finally start getting a practical grip on Python and that I hope can help make a great but often intimidating tool a bit more useful for archivists and the like. I'm going to give a lightning talk about wading into the waters of solo tool development at the BitCurator Forum in March, so do drop by that if you want to see a little bit of emulation-in-action but mostly want some more vaguely neurotic rhetorical questions about how digital preservation practice is going about it all wrong with staffing and resources!

At Home
---------

On the brighter side of things! My arc of personal comfort and sense of place actually continues to bend upwards despite the world being on fire. I got outside *a lot* - camping several times again, exploring the fairly extensive bike paths in the Valley, kayaking with my dad in Duxbury harbor, hiking (not continuously, but) the entire Holyoke range, plus Mts. Toby and Tom. And as the colder weather's set in, we started a gym membership (at an extremely relaxed, sparsely populated local joint) where I've been going about twice a week and feel like I'm finally settling into healthy adult habits and all that.

My grandmother passed away at the end of August - heartbreaking, but I admit I find it difficult to be "sad". She was 99, it was not unexpected, and she had an incredible life. She traveled a lot for someone in her generation and always found a lot of personal fulfillment in service to her community, from Indiana to Ontario to Massachusetts to Maryland to her retirement home, even (or especially) after my granddad passed some years back. I had a lovely last opportunity to spend time with her back in February, and she passed quietly in the company of family, finally/recently relieved of quite a bit of the discomfort she'd been feeling the last year or two. I've been going through some of her botanical journals recently, pressed flowers and studious, neat notes from her high school and college years, before she became a nurse; they're wonderful mementos of an inquisitive and thoughtful and curious person.

The highlight of the year was certainly taking out ten days in September for a belated "honeymoon" in Italy, spending three days in Naples and and six in Sorrento. Travel's still a privilege right now, and not something we decided to do lightly, but it truly knocked some part of my anxiously-wound brain loose to explore somewhere new, indulge in outrageous food, and just experience a completely different set of circumstances for a little while, away from work, America, all of it.

Runner-up was a dear high school friend's wedding in October, which was a lovely opportunity to finally introduce Carrie properly to the best people and places that defined the Cleveland chapter of my life.

Not that we're "back to normal" in the least. Carrie did finally come down with COVID in June, so that led to an extremely unfun couple of weeks and we're still grappling with the long-term ripple effects (I also missed my ten-year college reunion as a result, but TBH I'm still mulling over whether that wasn't a net positive).

I've reached a baseline where attending small- to mid-sized events, masked-up when indoors, is something where I can accept the risk and do without freaking out when it's in the name of friends, family, community and good vibes; work-related events and in-person conferences more or less remain off the table, and I'm pretty fine with that. In fact will I probably just try to keep that my baseline in the future regardless of airborne pathogens? This felt like a year of finding grounding in life outside of work: my marriage, my friends, my cat, my puzzles, my local movie theater, my woods, my reading. That's a balance I'm not actually sure I ever actually had even before 2020, and it's feeling pretty good :heart:

I'm going to wait a few more days yet before posting my media recommendations for the year, but a few other random/closing thoughts:

- Is _Animal Crossing_ done? I'd love to get into some kind of multiplayer/social game scene on my Switch but it seems like I missed the moment and no one wants my oranges :orange: :raccoon: What's the phenomenon du jour? Still _Among Us_?
- I had a ball this year re-discovering my cinephile instincts via a Mubi subscription. I may need to do a separate write-up just about the favorites I've discovered that way (summary: *Near Dark* **rules**)
- I placed around 95 out of 130 asynchronous (after-the-fact) solvers of the American Crossword Puzzle Tournament, which is juuust good enough that I'm seriously considering participating for real this next year in March. Why not???
